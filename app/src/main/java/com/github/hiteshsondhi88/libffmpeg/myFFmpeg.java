package com.github.hiteshsondhi88.libffmpeg;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;


import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.util.Map;

import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;


@SuppressWarnings("unused")
public class myFFmpeg implements FFmpegInterface {

    private final Context context;
    private myFFmpegExecuteAsyncTask ffmpegExecuteAsyncTask;
    private FFmpegLoadLibraryAsyncTask ffmpegLoadLibraryAsyncTask;

    private static final long MINIMUM_TIMEOUT = 10 * 1000;
    private long timeout = Long.MAX_VALUE;

    private SaveVideoThread saveVideoThread;
    private Bitmap outBitmap;

    public myFFmpeg(Context context) {
        this.context = context.getApplicationContext();
        Log.setDEBUG(Util.isDebug(this.context));
    }


    @Override
    public void loadBinary(FFmpegLoadBinaryResponseHandler ffmpegLoadBinaryResponseHandler) throws FFmpegNotSupportedException {
        String cpuArchNameFromAssets = null;
        switch (CpuArchHelper.getCpuArch()) {
            case x86:
                Log.i("Loading FFmpeg for x86 CPU");
                cpuArchNameFromAssets = "x86";
                break;
            case ARMv7:
                Log.i("Loading FFmpeg for armv7 CPU");
                cpuArchNameFromAssets = "armeabi-v7a";
                break;
            case ARMv7_NEON:
                Log.i("Loading FFmpeg for armv7-neon CPU");
                cpuArchNameFromAssets = "armeabi-v7a-neon";
                break;
            case NONE:
                throw new FFmpegNotSupportedException("Device not supported");
        }

        if (!TextUtils.isEmpty(cpuArchNameFromAssets)) {
            ffmpegLoadLibraryAsyncTask = new FFmpegLoadLibraryAsyncTask(context, cpuArchNameFromAssets, ffmpegLoadBinaryResponseHandler);
            ffmpegLoadLibraryAsyncTask.execute();
        } else {
            throw new FFmpegNotSupportedException("Device not supported");
        }
    }

    @Override
    public void execute(Map<String, String> environvenmentVars, String cmd, FFmpegExecuteResponseHandler ffmpegExecuteResponseHandler) throws FFmpegCommandAlreadyRunningException {
        if (ffmpegExecuteAsyncTask != null && !ffmpegExecuteAsyncTask.isProcessCompleted()) {
            throw new FFmpegCommandAlreadyRunningException("FFmpeg command is already running, you are only allowed to run single command at a time");
        }
        if (!cmd.isEmpty()) {
            String[] ffmpegBinary = new String[] { FileUtils.getFFmpeg(context, environvenmentVars) };
            String commandLine = ffmpegBinary[0] + " " + cmd;

            saveVideoThread = new SaveVideoThread(commandLine , timeout, ffmpegExecuteResponseHandler);
            saveVideoThread.start();
        } else {
            throw new IllegalArgumentException("shell command cannot be empty");
        }
    }

    public <T> T[] concatenate (T[] a, T[] b) {
        int aLen = a.length;
        int bLen = b.length;

        @SuppressWarnings("unchecked")
        T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);

        return c;
    }

    @Override
    public void execute(String cmd, FFmpegExecuteResponseHandler ffmpegExecuteResponseHandler) throws FFmpegCommandAlreadyRunningException {
        execute(null, cmd, ffmpegExecuteResponseHandler);
    }

    @Override
    public String getDeviceFFmpegVersion() throws FFmpegCommandAlreadyRunningException {
        /*
        ShellCommand shellCommand = new ShellCommand();
        CommandResult commandResult = shellCommand.runWaitFor(new String[] { FileUtils.getFFmpeg(context), "-version" });
        if (commandResult.success) {
            return commandResult.output.split(" ")[2];
        }
        // if unable to find version then return "" to avoid NPE
        */
        return "";
    }

    @Override
    public String getLibraryFFmpegVersion() {
        return context.getString(R.string.shipped_ffmpeg_version);
    }

    @Override
    public boolean isFFmpegCommandRunning() {
        return ffmpegExecuteAsyncTask != null && !ffmpegExecuteAsyncTask.isProcessCompleted();
    }

    @Override
    public boolean killRunningProcesses() {
        return Util.killAsync(ffmpegLoadLibraryAsyncTask) || Util.killAsync(ffmpegExecuteAsyncTask);
    }

    @Override
    public void setTimeout(long timeout) {
        if (timeout >= MINIMUM_TIMEOUT) {
            this.timeout = timeout;
        }
    }


    public void sendBitmapToStream(Bitmap bitmap) {
        outBitmap = bitmap;
        onResume();
    }


    //***********************************************************
    //******************** Save Video Thread ********************
    //***********************************************************

    class SaveVideoThread extends Thread {
        private String cmd;
        private FFmpegExecuteResponseHandler ffmpegExecuteResponseHandler;
        private ShellCommand shellCommand;
        private long timeout;
        public Process process;
        private long startTime;
        private String output = "";
        private boolean runFlag;
        OutputStream outputStream;

        Bitmap bitmap;

        public SaveVideoThread (String cmd, long timeout, FFmpegExecuteResponseHandler ffmpegExecuteResponseHandler) {

            this.cmd = cmd;
            this.timeout = timeout;
            this.ffmpegExecuteResponseHandler = ffmpegExecuteResponseHandler;
            this.shellCommand = new ShellCommand();

            startTime = System.currentTimeMillis();
            if (ffmpegExecuteResponseHandler != null) {
                ffmpegExecuteResponseHandler.onStart();
            }
        }


        @Override
        public void run() {
            try{
                process = shellCommand.run(cmd);
                if (process == null) {
                    //return CommandResult.getDummyFailureResponse();
                }
                else {
                    outputStream = process.getOutputStream();

                    bitmap = Bitmap.createBitmap(80, 60, Bitmap.Config.ARGB_8888);
                    for (int i = 0; i < 80 * 50; i++) {
                        bitmap.setPixel(i % 80, i / 80, Color.RED);
                    }
                }


                while(!Thread.currentThread().isInterrupted()) {
                    // Frame ready to be added to the video
                    onPause();
                    while(runFlag != true)
                        SystemClock.sleep(1);

                    ByteBuffer bf = ByteBuffer.allocate( outBitmap.getHeight()*outBitmap.getWidth()*4 );
                    outBitmap.copyPixelsToBuffer(bf);
                    outputStream.write(bf.array());
                }

            }catch (Exception e) {
                e.printStackTrace();
            }


        }
    }



    /**
     * Call this on pause.
     */
    private void onPause() {
        saveVideoThread.runFlag = false;
    }

    /**
     * Call this on resume.
     */
    private void onResume() {
        saveVideoThread.runFlag = true;
    }

    /**
     * Call this on stop.
     */
    public void onStop() {
        if(saveVideoThread != null) {
            saveVideoThread.interrupt();
        }
        try {
            saveVideoThread.outputStream.close();
        }catch (IOException ioe) {
            ioe.printStackTrace();
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 200ms
                Util.destroyProcess(saveVideoThread.process);
            }
        }, 200);
    }
}
