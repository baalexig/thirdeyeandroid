package by.axonim.te_phone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Alex on 11/16/2015.
 */
public class WifiListAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<WifiListItem> objects;

    WifiListAdapter(Context context, ArrayList<WifiListItem> wifiListItems) {
        ctx = context;
        objects = wifiListItems;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.wifi_list_item, parent, false);
        }

        WifiListItem item = getWifiItem(position);

        // заполняем View в пункте списка данными из товаров: наименование, цена
        // и картинка
        ((TextView) view.findViewById(R.id.nameApTextView)).setText(item.ssid);
        ((TextView) view.findViewById(R.id.connectionTextView)).setText(item.isConnected);

        //((TextView) view.findViewById(R.id.tvPrice)).setText(item.price + "");
        switch (item.signalStrenght){
            case 0:
                ((ImageView) view.findViewById(R.id.ivImage)).setImageResource(R.drawable.ic_wifi0);
                break;
            case 1:
                ((ImageView) view.findViewById(R.id.ivImage)).setImageResource(R.drawable.ic_wifi1);
                break;
            case 2:
                ((ImageView) view.findViewById(R.id.ivImage)).setImageResource(R.drawable.ic_wifi2);
                break;
            case 3:
                ((ImageView) view.findViewById(R.id.ivImage)).setImageResource(R.drawable.ic_wifi3);
                break;
            case 4:
                ((ImageView) view.findViewById(R.id.ivImage)).setImageResource(R.drawable.ic_wifi4);
                break;
        }


        return view;
    }

    // товар по позиции
    WifiListItem getWifiItem(int position) {
        return ((WifiListItem) getItem(position));
    }
}
