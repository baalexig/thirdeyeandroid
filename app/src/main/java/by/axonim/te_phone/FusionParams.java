package by.axonim.te_phone;

import android.content.res.Configuration;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;

/**
 * Created by Alex on 11/23/2015.
 */
public class FusionParams implements View.OnTouchListener {
    private boolean inTouch = false;
    private boolean doubleTouch = false;

    private float scaleInitial;
    private float firstCoord[] = new float[4];
    private float firstOffset[] = new float[2];
    private CameraProcessing camProcessing;
    private int dispOrientation;

    private float doubleDistanceP;

    FusionParams(SurfaceView surface, CameraProcessing cP, int orientation) {
        dispOrientation = orientation;
        camProcessing = cP;
        surface.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        // событие
        int actionMask = event.getActionMasked();
        // индекс касания
        int pointerIndex = event.getActionIndex();
        // число касаний
        int pointerCount = event.getPointerCount();

        switch (actionMask) {
            case MotionEvent.ACTION_DOWN: // первое касание
                // save first tangency point
                if(dispOrientation == Configuration.ORIENTATION_PORTRAIT) {
                    firstCoord[0] = event.getX(0);
                    firstCoord[1] = event.getY(0);
                } else {
                    firstCoord[0] = -event.getY(0);
                    firstCoord[1] = event.getX(0);
                }
                firstOffset[0] = camProcessing.getCameraXOffset();
                firstOffset[1] = camProcessing.getCameraYOffset();
                inTouch = true;
            case MotionEvent.ACTION_POINTER_DOWN: // последующие касания
                if( (pointerCount >= 2) && (!doubleTouch) ) {
                    doubleTouch = true;

                    // save tangency points
                    firstCoord[0] = event.getX(0);
                    firstCoord[1] = event.getY(0);
                    firstCoord[2] = event.getX(1);
                    firstCoord[3] = event.getY(1);

                    // calculate distance between points
                    float dXi = firstCoord[0] - firstCoord[2];
                    float dYi = firstCoord[1] - firstCoord[3];
                    doubleDistanceP = (float) Math.sqrt( dXi*dXi + dYi*dYi );

                    scaleInitial = camProcessing.getCameraScale();
                }
                break;

            case MotionEvent.ACTION_UP: // прерывание последнего касания
                inTouch = false;
            case MotionEvent.ACTION_POINTER_UP: // прерывания касаний
                doubleTouch = false;
                break;


            case MotionEvent.ACTION_MOVE: // движение
                if(inTouch) {
                    if( !doubleTouch ) {
                        //  Set Offset
                        int xOffset;
                        int yOffset;
                        if(dispOrientation == Configuration.ORIENTATION_PORTRAIT) {
                            xOffset = (int) (firstOffset[0] + (firstCoord[0] - event.getX()) * 0.2f);
                            yOffset = (int) (firstOffset[1] + (firstCoord[1] - event.getY()) * 0.2f);
                        } else {
                            xOffset = (int) (firstOffset[0] + (firstCoord[0] + event.getY()) * 0.2f);
                            yOffset = (int) (firstOffset[1] + (firstCoord[1] - event.getX()) * 0.2f);
                        }
                        camProcessing.setCameraOffset(xOffset, yOffset);
                    } else {
                        //  Set Scale
                        float dXc = event.getX(0) - event.getX(1);
                        float dYc = event.getY(0) - event.getY(1);
                        float scale = scaleInitial * ((float) Math.sqrt( dXc*dXc + dYc*dYc ) / doubleDistanceP);
                        camProcessing.setCameraScale(scale);
                        //Log.i("Touch", "Scale = " + String.valueOf(scale) );
                    }
                }
                break;
        }

        return true;
    }
}
