package by.axonim.te_phone;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.googlecode.javacv.cpp.opencv_core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;


/**
 * Created by Alex on 5/25/2015.
 */

class CameraProcessing extends SurfaceView implements SurfaceHolder.Callback,  Camera.PictureCallback, Camera.PreviewCallback, Camera.AutoFocusCallback
{
    private Camera mCamera;
    SurfaceHolder mHolder;
    private SurfaceView mSurface;



    private int dispOrientation;
    private float leptonHorizontalAngle;
    private float leptonVerticalAngle;
    private float cameraHorizontalAngle;
    private float cameraVerticalAngle;
    private Camera.Size cameraSize;
    private DisplayMetrics displayMetrics;
    private int cameraWidth = 320;
    private int cameraHeight = 240;
    private boolean isInitialized = false;

    private int cameraXOffset = 0;
    private int cameraYOffset = 0;
    private float cameraScale = 1.0f;


    private Context mContext;


    private opencv_core.IplImage nv21CameraIplimage = null;
    private opencv_core.IplImage grayCameraIplimage = null;
    private opencv_core.IplImage grayRotCameraIplimage = null;
    private opencv_core.IplImage grayCameraResizedIplimage = null;
    private opencv_core.IplImage grayCameraIplimageDisplay=null;
    private opencv_core.IplImage rgbaLeptIplimage = null;
    private opencv_core.IplImage grayLeptIplimage = null;
    private opencv_core.IplImage grayLeptIplimageResized = null;
    private opencv_core.IplImage resIplimage=null;
    private Bitmap bitmap;
    private Bitmap leptonBitmap;
    private boolean surfaceCreated = false;
    CvPoint minloc, maxloc;


    public CameraProcessing(Context context,
                            SurfaceView surfaceView,
                            Camera camera,
                            float LeptonHorizontalViewAngle,
                            float LeptonVerticalViewAndle,
                            DisplayMetrics metrics,
                            int orientation) {
        super(context);
        mContext = context;
        mCamera = camera;
        dispOrientation = orientation;

        displayMetrics = metrics;
        leptonHorizontalAngle = LeptonHorizontalViewAngle;
        leptonVerticalAngle = LeptonVerticalViewAndle;

        // Create SurfaceView
        mSurface = surfaceView;

        mHolder = mSurface.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }




    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try
        {
            mCamera.setPreviewDisplay(mHolder);
            mSurface.setZOrderOnTop(true);
            SurfaceHolder h = holder;
            h.setFormat(PixelFormat.TRANSPARENT);
            mCamera.setPreviewCallback(this);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        Camera.Parameters cameraParams = mCamera.getParameters();
        cameraHorizontalAngle = cameraParams.getHorizontalViewAngle();
        cameraVerticalAngle = cameraParams.getVerticalViewAngle();
        cameraSize = getOptimalPreviewSize(cameraParams.getSupportedPreviewSizes(), 640, 480);
        cameraParams.setPreviewSize(cameraSize.width, cameraSize.height);
        cameraWidth = cameraSize.width;
        cameraHeight = cameraSize.height;
        //cameraParams.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
        mCamera.setParameters(cameraParams);

        nv21CameraIplimage = opencv_core.IplImage.create(cameraSize.width, cameraSize.height * 3 / 2, IPL_DEPTH_8U, 1);
        grayCameraIplimage = opencv_core.IplImage.create(cameraSize.width, cameraSize.height, IPL_DEPTH_8U, 1);
        grayRotCameraIplimage = opencv_core.IplImage.create(grayCameraIplimage.height(), grayCameraIplimage.width(), IPL_DEPTH_8U, 1);


        float k = cameraHorizontalAngle / leptonHorizontalAngle;
        float gcdCameraDimensions = (float) gcdThing(cameraSize.width, cameraSize.height);
        k = (float) Math.round(k*gcdCameraDimensions) / gcdCameraDimensions;

        if(dispOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            grayCameraResizedIplimage = opencv_core.IplImage.create(
                    (int) (cameraSize.width * k),
                    (int) (cameraSize.height * k),
                    IPL_DEPTH_8U, 1
            );

            int widthDisplayImage = (int) (displayMetrics.widthPixels * k);
            int heightDisplayImage = (int) (displayMetrics.widthPixels * k * cameraSize.height / cameraSize.width);
            grayCameraIplimageDisplay = opencv_core.IplImage.create(widthDisplayImage, heightDisplayImage, IPL_DEPTH_8U, 1);
            //bitmap = Bitmap.createBitmap(grayCameraResizedIplimage.width(), grayCameraResizedIplimage.height(), Bitmap.Config.ALPHA_8);
            bitmap = Bitmap.createBitmap(grayCameraIplimage.width(), grayCameraIplimage.height(), Bitmap.Config.ALPHA_8);


            rgbaLeptIplimage = opencv_core.IplImage.create(80, 60, IPL_DEPTH_8U, 4);
            grayLeptIplimage = opencv_core.IplImage.create(80, 60, IPL_DEPTH_8U, 1);
            grayLeptIplimageResized = opencv_core.IplImage.create(cameraSize.width, 60 * cameraSize.width / 80, IPL_DEPTH_8U, 1);


            resIplimage = IplImage.create((grayCameraResizedIplimage.width() - grayLeptIplimageResized.width() + 1),
                    (grayCameraResizedIplimage.height() - grayLeptIplimageResized.height() + 1),
                    IPL_DEPTH_32F,
                    1
            );
        }
        else {
            /*
            grayCameraResizedIplimage = opencv_core.IplImage.create(
                    (int) (cameraSize.height * k),
                    (int) (cameraSize.width * k),
                    IPL_DEPTH_8U, 1
            );
            */
            grayCameraResizedIplimage = opencv_core.IplImage.create(
                    (264),
                    (352),
                    IPL_DEPTH_8U, 1
            );

            int widthDisplayImage = (int) (displayMetrics.heightPixels * k);
            int heightDisplayImage = (int) (displayMetrics.heightPixels * k * cameraSize.height / cameraSize.width);
            grayCameraIplimageDisplay = opencv_core.IplImage.create(heightDisplayImage, widthDisplayImage, IPL_DEPTH_8U, 1);
            //bitmap = Bitmap.createBitmap(heightDisplayImage, widthDisplayImage, Bitmap.Config.ALPHA_8);
            //bitmap = Bitmap.createBitmap(grayCameraResizedIplimage.width(), grayCameraResizedIplimage.height(), Bitmap.Config.ALPHA_8);
            bitmap = Bitmap.createBitmap(grayRotCameraIplimage.width(), grayRotCameraIplimage.height(), Bitmap.Config.ALPHA_8);
            //grayCameraIplimageDisplay = opencv_core.IplImage.create(heightDisplayImage, widthDisplayImage, IPL_DEPTH_8U, 1);
            //bitmap = Bitmap.createBitmap(heightDisplayImage, widthDisplayImage, Bitmap.Config.ALPHA_8);
            //bitmap = Bitmap.createBitmap(grayRotCameraIplimage.width(), grayRotCameraIplimage.height(), Bitmap.Config.ALPHA_8);


            rgbaLeptIplimage = opencv_core.IplImage.create(60, 80, IPL_DEPTH_8U, 4);
            grayLeptIplimage = opencv_core.IplImage.create(60, 80, IPL_DEPTH_8U, 1);
            grayLeptIplimageResized = opencv_core.IplImage.create(60 * cameraSize.width / 80, cameraSize.width, IPL_DEPTH_8U, 1);


//            resIplimage = IplImage.create(
//                    (grayCameraResizedIplimage.width() - grayLeptIplimageResized.width() + 1),
//                    (grayCameraResizedIplimage.height() - grayLeptIplimageResized.height() + 1),
//                    IPL_DEPTH_32F,
//                    1
//            );
        }
        //resGrayIplimage = IplImage.create(  (image.getWidth()-templ.getWidth()+1), (image.getHeight()-templ.getHeight()+1), IPL_DEPTH_8U, 1 );


        Camera.Size previewSize = mCamera.getParameters().getPreviewSize();
        float aspect = (float) previewSize.width / previewSize.height;

        int previewSurfaceWidth = mSurface.getWidth();
        int previewSurfaceHeight = mSurface.getHeight();

        ViewGroup.LayoutParams lp = mSurface.getLayoutParams();

        // здесь корректируем размер отображаемого preview, чтобы не было искажений

        if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE)
        {
            // портретный вид
            mCamera.setDisplayOrientation(0);
            lp.height = 1;
            lp.width = 1;
            //lp.height = previewSize.width;
            //lp.width = previewSize.height;
        }
        else
        {
            // ландшафтный
            mCamera.setDisplayOrientation(0);
            lp.width = 1;
            lp.height = 1;
            //lp.width = previewSize.width;
            //lp.height = previewSize.height;
        }

        mSurface.setLayoutParams(lp);
        mCamera.startPreview();
        surfaceCreated = true;
        isInitialized = true;
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }


    @Override
    public void onPictureTaken(byte[] paramArrayOfByte, Camera paramCamera)
    {
        // сохраняем полученные jpg в папке /sdcard/CameraExample/
        // имя файла - System.currentTimeMillis()

        try
        {
            File saveDir = new File("/sdcard/CameraExample/");

            if (!saveDir.exists())
            {
                saveDir.mkdirs();
            }

            FileOutputStream os = new FileOutputStream(String.format("/sdcard/CameraExample/%d.jpg", System.currentTimeMillis()));
            os.write(paramArrayOfByte);
            os.close();
        }
        catch (Exception e)
        {
        }

        // после того, как снимок сделан, показ превью отключается. необходимо включить его
        paramCamera.startPreview();
    }

    @Override
    public void onAutoFocus(boolean paramBoolean, Camera paramCamera)
    {
        if (paramBoolean)
        {
            // если удалось сфокусироваться, делаем снимок
            paramCamera.takePicture(null, null, null, this);
        }
    }

    @Override
    public void onPreviewFrame(byte[] paramArrayOfByte, Camera paramCamera)
    {
        // здесь можно обрабатывать изображение, показываемое в preview

        long prevTime = System.currentTimeMillis();

        nv21CameraIplimage.getByteBuffer().put(paramArrayOfByte);

        cvCvtColor(nv21CameraIplimage, grayCameraIplimage, CV_YUV2GRAY_NV21);
        //cvSmooth(grayIplimage, grayIplimage, CV_BLUR, 3);
        if(dispOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            cvLaplace(grayCameraIplimage, grayCameraIplimage, 3);
            //cvResize(grayCameraIplimage, grayCameraIplimageDisplay, CV_INTER_LINEAR);
            //bitmap.copyPixelsFromBuffer(grayCameraIplimageDisplay.getByteBuffer());
            //cvResize(grayCameraIplimage, grayCameraResizedIplimage, CV_INTER_LINEAR);
            bitmap.copyPixelsFromBuffer(grayCameraIplimage.getByteBuffer());
        }
        else {
            cvTranspose(grayCameraIplimage, grayRotCameraIplimage);

            cvFlip(grayRotCameraIplimage, grayRotCameraIplimage, 1);
            cvLaplace(grayRotCameraIplimage, grayRotCameraIplimage, 3);
            //cvResize(grayRotCameraIplimage, grayCameraIplimageDisplay, CV_INTER_LINEAR);
            //bitmap.copyPixelsFromBuffer(grayCameraIplimageDisplay.getByteBuffer());
            //cvResize(grayRotCameraIplimage, grayCameraResizedIplimage, CV_INTER_LINEAR);
            bitmap.copyPixelsFromBuffer(grayRotCameraIplimage.getByteBuffer());
        }




        //bitmap.copyPixelsFromBuffer(grayCameraIplimageDisplay.getByteBuffer());
        long sobelTime = System.currentTimeMillis() - prevTime;
        Log.i("sobelTime", String.valueOf(sobelTime));


        /*
        nv21CameraIplimage.getByteBuffer().put(paramArrayOfByte);
        cvCvtColor(nv21CameraIplimage, grayCameraIplimage, CV_YUV2GRAY_NV21);
        if(dispOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            //cvSmooth(grayIplimage, grayIplimage, CV_BLUR, 3);
            cvLaplace(grayCameraIplimage, grayCameraIplimage, 3);
            cvResize(grayCameraIplimage, grayCameraResizedIplimage, CV_INTER_LINEAR);
            cvResize(grayCameraIplimage, grayCameraIplimageDisplay, CV_INTER_LINEAR);
        }
        else {
            cvTranspose(grayCameraIplimage, grayRotCameraIplimage);
            cvFlip(grayRotCameraIplimage, null, 1);
            cvLaplace(grayRotCameraIplimage, grayRotCameraIplimage, 3);
            cvResize(grayRotCameraIplimage, grayCameraResizedIplimage, CV_INTER_LINEAR);
            cvResize(grayRotCameraIplimage, grayCameraIplimageDisplay, CV_INTER_LINEAR);
        }


        cvCvtColor(rgbaLeptIplimage, grayLeptIplimage, CV_RGBA2GRAY);
        cvLaplace(grayLeptIplimage, grayLeptIplimage, 3);
        cvResize(grayLeptIplimage, grayLeptIplimageResized, CV_INTER_LINEAR);



        cvMatchTemplate(grayCameraResizedIplimage, grayLeptIplimageResized, resIplimage, CV_TM_SQDIFF);


        double[] minval = new double[1];
        double[] maxval = new double[1];



        minloc = new CvPoint();
        maxloc = new CvPoint();

        cvMinMaxLoc(resIplimage, minval, maxval, minloc, maxloc, null);


        //cvCvtColor(grayResizedIplimage, rgbaIplimage, CV_GRAY2RGBA);


        Log.i( "min x", String.valueOf(minloc.x()) );
        Log.i( "min y", String.valueOf(minloc.y()) );



        //bitmap.copyPixelsFromBuffer(rgbaIplimage.getByteBuffer());

        bitmap.copyPixelsFromBuffer(grayCameraIplimageDisplay.getByteBuffer());

        //imageView.setImageBitmap(bitmap);
        */
    }

    public void setBitmap(Bitmap btmp) {
        /*
        //IplImage temp = IplImage.create(80, 60, );
        if(rgbaLeptIplimage != null)
            btmp.copyPixelsToBuffer(rgbaLeptIplimage.getByteBuffer());
        */
    }

    public Bitmap getBitmap ()
    {
        /*
        long prevTime = System.currentTimeMillis();
        cvCvtColor(nv21Iplimage, grayIplimage, CV_YUV2GRAY_NV21);
        //cvSmooth(grayIplimage, grayIplimage, CV_BLUR, 3);
        cvLaplace(grayIplimage, grayIplimage, 3);
        cvResize(grayIplimage, grayIplimageLarge, CV_INTER_LINEAR);


        long sobelTime = System.currentTimeMillis() - prevTime;
        Log.i("sobelTime", String.valueOf(sobelTime));

        bitmap.copyPixelsFromBuffer(grayIplimageLarge.getByteBuffer());
        */
        return bitmap;
    }


    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio=(double)h / w;

        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    private static int gcdThing(int a, int b) {
        BigInteger b1 = BigInteger.valueOf(a);
        BigInteger b2 = BigInteger.valueOf(b);
        BigInteger gcd = b1.gcd(b2);
        return gcd.intValue();
    }

    public int getCameraXOffset() {return cameraXOffset;}
    public int getCameraYOffset() {return cameraYOffset;}
    public void setCameraOffset(int x, int y) {
        cameraXOffset = x;
        cameraYOffset = y;
    }

    public void setCameraScale(float scale) {
        cameraScale = scale;
    }

    public float getCameraScale(){
        return cameraScale;
    }

    public boolean ifSurfaceCreated (){
        return surfaceCreated;
    }

    public IplImage getNv21Iplimage (){
        return nv21CameraIplimage;
    }

    public CvPoint getLoc()
    {
        return minloc;
    }
    public int getCameraWidth() {return cameraWidth; }
    public int getCameraHeight() {return cameraHeight; }
    public boolean isInitialized() {return isInitialized;}
}
