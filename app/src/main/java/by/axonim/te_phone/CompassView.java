package by.axonim.te_phone;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


import java.util.concurrent.TimeUnit;

/**
 * Created by Alex on 5/25/2015.
 */

class CompassView extends SurfaceView implements SurfaceHolder.Callback
{
    private float averageAzimuth = 0.0f;

    /** Various dimensions and other drawing-related constants. */
    private static final float TICK_WIDTH = 2;
    private float TICK_HEIGHT;
    private float LONG_TICK_HEIGHT;
    private float DIRECTION_TEXT_HEIGHT;
    private float TRIANGLE_WIDTH;
    private float TRIANGLE_HEIGHT;


    private float VIEW_WIDTH;
    private float VIEW_HEIGHT;

    private float VIEW_ANGLE;
    private static final float VIEW_ANGLE_PORTRAIT = 45.0f;
    private static final float VIEW_ANGLE_LANDSCAPE = 90.0f;

    /** The refresh rate, in frames per second, of the compass. */
    private static final int REFRESH_RATE_FPS = 30;

    /** How often long ticks drawn */
    private static final int DEGREES_PER_TICK = 10;

    /** The duration, in milliseconds, of one frame. */
    private static final long FRAME_TIME_MILLIS = TimeUnit.SECONDS.toMillis(1) / REFRESH_RATE_FPS;


    private Paint mPaint;
    private Paint mTickPaint;
    private Paint mTrianglePaint;
    private Path mTrianglePath;
    private Rect mTextBounds;
    private String[] mDirections;
    private float pixelsPerDegree;
    private Bitmap mDirectionsBitmap;

    public DrawThread drawThread;
    private SurfaceView mSurface;

    private LinearGradient horizontal;
    private SurfaceHolder surfaceHolder;
    private Context mContext;


    public CompassView(Context context, SurfaceView surfaceView) {
        super(context);
        mContext = context;
        if( context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            VIEW_ANGLE = VIEW_ANGLE_LANDSCAPE;
        else
            VIEW_ANGLE = VIEW_ANGLE_PORTRAIT;

        // Create SurfaceView
        mSurface = surfaceView;
        mSurface.setZOrderOnTop(true);

        SurfaceHolder mHolder = mSurface.getHolder();
        mHolder.addCallback(this);

        //  Set transparent background
        mHolder.setFormat(PixelFormat.TRANSPARENT);
    }





    public void updateAzimuth(float originalAzimuth) {
        averageAzimuth = originalAzimuth;
    }

    public String getCompassString() {
        String compassString = "";
        if( (averageAzimuth > 337.5f) || (averageAzimuth <= 22.5f) )
            compassString = String.format("%.0f", averageAzimuth) + " N";
        if( (averageAzimuth > 22.5f) && (averageAzimuth <= 67.5f) )
            compassString = String.format("%.0f", averageAzimuth) + " NE";
        if( (averageAzimuth > 67.5f) && (averageAzimuth <= 112.5f) )
            compassString = String.format("%.0f", averageAzimuth) + " E";
        if( (averageAzimuth > 112.5f) && (averageAzimuth <= 157.5f) )
            compassString = String.format("%.0f", averageAzimuth) + " SE";
        if( (averageAzimuth > 157.5f) && (averageAzimuth <= 202.5f) )
            compassString = String.format("%.0f", averageAzimuth) + " S";
        if( (averageAzimuth > 202.5f) && (averageAzimuth <= 247.5f) )
            compassString = String.format("%.0f", averageAzimuth) + " SW";
        if( (averageAzimuth > 247.5f) && (averageAzimuth <= 292.5f) )
            compassString = String.format("%.0f", averageAzimuth) + " W";
        if( (averageAzimuth > 292.5f) && (averageAzimuth <= 337.5f) )
            compassString = String.format("%.0f", averageAzimuth) + " NW";
        return compassString;
    }


    /**
     * Draws the compass direction strings (N, NW, W, etc.).
     *
     */
    public void drawCompassBitmap() {
        // The view displays 90 degrees across its width so that one 90 degree head rotation is
        // equal to one full view cycle.
        TICK_HEIGHT = VIEW_HEIGHT * 0.087f;
        LONG_TICK_HEIGHT = VIEW_HEIGHT * 0.175f;
        DIRECTION_TEXT_HEIGHT = VIEW_HEIGHT * 0.224f;
        TRIANGLE_HEIGHT = VIEW_HEIGHT * 0.238f;
        TRIANGLE_WIDTH = TRIANGLE_HEIGHT * 0.8f;

        //
        //  Make a gradient for compass view
        //  Transparent left and right
        //
        int[] linColors = {0x2fffffff, 0xffffffff, 0xffffffff, 0x2fffffff};
        float[] linPositions = {0, 0.4f, 0.6f, 1};
        horizontal = new LinearGradient(0, 0, VIEW_WIDTH, 0, linColors, linPositions, Shader.TileMode.CLAMP);


        //  Make a triangle path
        Point a = new Point( (int) ((VIEW_WIDTH+TRIANGLE_WIDTH)/2), (int) VIEW_HEIGHT );
        Point b = new Point( (int) ((VIEW_WIDTH-TRIANGLE_WIDTH)/2), (int) VIEW_HEIGHT );
        Point c = new Point( (int) (VIEW_WIDTH/2), (int) (VIEW_HEIGHT - TRIANGLE_HEIGHT));

        mTrianglePath.setFillType(Path.FillType.EVEN_ODD);
        mTrianglePath.moveTo(a.x, a.y);
        mTrianglePath.lineTo(b.x, b.y);
        mTrianglePath.lineTo(c.x, c.y);
        mTrianglePath.lineTo(a.x, a.y);
        mTrianglePath.close();


        mPaint.setTextSize(DIRECTION_TEXT_HEIGHT);

        //
        //  Create bitmap with angle 360 + 2*viewangle
        //
        mDirectionsBitmap = Bitmap.createBitmap( (int) ((360f/VIEW_ANGLE + 2.0f)*VIEW_WIDTH), (int) VIEW_HEIGHT, Bitmap.Config.ARGB_8888 );
        Canvas tempCanvas = new Canvas(mDirectionsBitmap);


        // The view displays 45 degrees across its width so that one 90 degree head rotation is
        // equal to one full view cycle.
        pixelsPerDegree = VIEW_WIDTH / VIEW_ANGLE;
        float pixelsPerTick = pixelsPerDegree * DEGREES_PER_TICK;
        float startDegreeOffset = VIEW_WIDTH % pixelsPerDegree;
        float startTickOffset = VIEW_WIDTH % pixelsPerTick;
        int offsetTickInDegree = Math.round( Math.abs(startTickOffset - startDegreeOffset) / pixelsPerDegree);

        mPaint.setAlpha(255);
        mTickPaint.setAlpha(255);
        //
        //     Draw ticks, directions
        //
        float widthCoor = startDegreeOffset;
        for(int i = 1; i <= (360 + 2*VIEW_ANGLE); i++)
        {
            widthCoor += pixelsPerDegree;

            if( (i%DEGREES_PER_TICK) == offsetTickInDegree ) {
                tempCanvas.drawLine(widthCoor, VIEW_HEIGHT,
                        widthCoor, VIEW_HEIGHT - LONG_TICK_HEIGHT, mTickPaint);
                // Draw a text label for the even indices.
                //String direction = mDirections[i/DEGREES_PER_TICK];

                int intDirection = Math.round(((float)i - VIEW_ANGLE) / 10f)*10 % 360;
                if (intDirection < 0)
                    intDirection += 360;


                String strDirection;
                if ((intDirection % 90) == 0) {
                    mPaint.setColor(Color.RED);
                    strDirection = mDirections[intDirection / 90 * 2];
                }
                else {
                    mPaint.setColor(Color.WHITE);
                    strDirection = Integer.toString(intDirection);
                }

                mPaint.getTextBounds(strDirection, 0, strDirection.length(), mTextBounds);

                tempCanvas.drawText(strDirection,
                        widthCoor - mTextBounds.width() / 2,
                        (VIEW_HEIGHT + mTextBounds.height()) / 2, mPaint);

            }
            else
                tempCanvas.drawLine(widthCoor , VIEW_HEIGHT,
                        widthCoor, VIEW_HEIGHT - TICK_HEIGHT, mTickPaint );
        }
    }

    private void clearCompassCanvas (Canvas canvas) {
        if(canvas == null)
            return;

        // Clear canvas
        Paint clearPaint = new Paint();
        clearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), clearPaint);
    }


    private void drawCompassBitmapOnCanvas(Canvas canvas, float angle) {
        if(canvas == null)
            return;

        clearCompassCanvas(canvas);

        Rect RectSrc = new Rect( (int) (( angle%360 + VIEW_ANGLE)*pixelsPerDegree - VIEW_WIDTH/2),
                0,
                (int) (VIEW_WIDTH/2 + (angle%360 + VIEW_ANGLE)*pixelsPerDegree),
                (int) VIEW_HEIGHT + 2);

        Rect RectDst = new Rect(0, 0, (int) VIEW_WIDTH, (int) VIEW_HEIGHT);


        Paint bitmapPaint = new Paint();

        //
        // Draw Directions
        //
        canvas.drawBitmap(mDirectionsBitmap, RectSrc, RectDst, bitmapPaint);

        //
        //  Draw triangle aim
        //
        canvas.drawPath(mTrianglePath, mTrianglePaint);


        bitmapPaint.setShader(horizontal);
        bitmapPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY));
        canvas.drawRect(new RectF(0, 0, VIEW_WIDTH, VIEW_HEIGHT), bitmapPaint);
        //canvas.drawBitmap(bitmap, RectDst, RectDst, paint);
    }


    private void updateDrawings(){
        mTrianglePath = new Path();

        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setAntiAlias(true);
        mPaint.setTextSize(DIRECTION_TEXT_HEIGHT);
        mPaint.setColor(Color.WHITE);
        //mPaint.setTypeface(Typeface.create("sans-serif-thin", Typeface.NORMAL));


        mTickPaint = new Paint();
        mTickPaint.setStyle(Paint.Style.STROKE);
        mTickPaint.setStrokeWidth(TICK_WIDTH);
        mTickPaint.setAntiAlias(true);
        mTickPaint.setColor(Color.WHITE);


        mTrianglePaint = new Paint();
        mTrianglePaint.setStrokeWidth(1);
        mTrianglePaint.setColor(Color.WHITE);
        mTrianglePaint.setStyle(Paint.Style.FILL);
        mTrianglePaint.setAntiAlias(true);

        mTextBounds = new Rect();

        mDirections = mContext.getResources().getStringArray(R.array.direction_abbreviations);
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceHolder = holder;
        drawThread = new DrawThread(holder, getResources());

        Canvas canvas = holder.lockCanvas();
        VIEW_WIDTH = canvas.getWidth();
        VIEW_HEIGHT = canvas.getHeight();
        holder.unlockCanvasAndPost(canvas);

        updateDrawings();
        drawCompassBitmap();

        drawThread.start();
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if(drawThread != null) {
            drawThread.interrupt();
            drawThread = null;
        }
    }



    class DrawThread extends Thread{
        private boolean mPaused;
        private Object mPauseLock;
        private SurfaceHolder surfaceHolder;
        private Matrix matrix;
        private Paint paint;
        float a = 0;

        public DrawThread(SurfaceHolder surfaceHolder, Resources resources) {
            this.surfaceHolder = surfaceHolder;

            mPauseLock = new Object();
            mPaused = false;

            matrix = new Matrix();
            matrix.postScale(1.0f, 1.0f);
            matrix.postTranslate(20.0f, 20.0f);

            paint = new Paint();
            paint.setFilterBitmap(true);
        }


        @Override
        public void run() {
            Canvas canvas;
            while (!Thread.currentThread().isInterrupted()) {
                canvas = null;
                try {
                    long frameStart = SystemClock.elapsedRealtime();

                    canvas = surfaceHolder.lockCanvas(null);
                    synchronized (surfaceHolder) {
                        //a += 0.1f;
                        if(!mPaused) {
                            drawCompassBitmapOnCanvas(canvas, averageAzimuth);
                        }
                    }


                    long frameLength = SystemClock.elapsedRealtime() - frameStart;

                    long sleepTime = FRAME_TIME_MILLIS - frameLength;
                    if (sleepTime > 0) {
                        SystemClock.sleep(sleepTime);
                    }

                } finally {
                    if (canvas != null) {
                        // отрисовка выполнена. выводим результат на экран
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }


                synchronized (mPauseLock) {
                    while (mPaused) {
                        canvas = null;
                        try {
                            canvas = surfaceHolder.lockCanvas(null);
                            synchronized (surfaceHolder) {
                                clearCompassCanvas(canvas);
                            }
                        } finally {
                            if (canvas != null) {
                                surfaceHolder.unlockCanvasAndPost(canvas);
                            }
                        }
                        try {
                            mPauseLock.wait();
                        } catch (InterruptedException e) {
                        }
                    }
                }

            }
        }

        /**
         * Call this on pause.
         */
        public void onPause() {
            synchronized (mPauseLock) {
                mPaused = true;
            }
        }

        /**
         * Call this on resume.
         */
        public void onResume() {
            synchronized (mPauseLock) {
                mPaused = false;
                mPauseLock.notifyAll();
            }
        }
    }
}