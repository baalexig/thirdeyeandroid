package by.axonim.te_phone;

/**
 * Created by Alex on 11/16/2015.
 */
public class WifiListItem {
    String ssid;
    String isConnected;
    int signalStrenght;

    WifiListItem(String _ssid, String _isConnected, int _signalStrenght) {
        ssid = _ssid;
        isConnected = _isConnected;
        signalStrenght = _signalStrenght;
    }
}
