package by.axonim.te_phone;

import android.content.res.Configuration;
import android.util.Log;

/**
 * Created by Alex on 2/20/2015.
 */
public class Lepton {


    private int[] TempArray;
    private int[] RawImage;
    private int[] ColorImage;
    private int[] revColorImage;
    public float aimTemp;
    public float minTemp;
    public float maxTemp;
    public float tempFPA;
    public float tempAUX;
    private int AverageTemp;
    private float shutterAuxDT = 0;
    private float minSelectedTemp = 30f;
    private float maxSelectedTemp = 40f;

    private int min, max;


    private int displayOrientation;

    /** Horizontal image angle */
    private static final float HORIZONTAL_ANGLE = 50f;

    /** Vertical image angle */
    private static final float VERTICAL_ANGLE = 39f;

    /** Horizontal image angle */
    private int WIDTH = 160;

    /** Vertical image angle */
    private int HEIGHT = 120;


    private int[] intIronBow;
    private int[] intRainBow;
    private int[] intGray;
    private int[] intInvGray;


    private float a1 = -0.00063451f;
    private float a2 = 0.3104458f;

    private float b1 = -0.21711288f;
    private float b2 = 86.836963f;


    private float c1 = 0.1898452f;
    private float c2 = -131.970741f;
    private float c3 = 30345.076543f;


    public Lepton(int orientation){
        displayOrientation = orientation;

        TempArray = new int[WIDTH*HEIGHT];
        RawImage = new int[WIDTH*HEIGHT];
        ColorImage = new int[WIDTH*HEIGHT];
        revColorImage = new int[WIDTH*HEIGHT];


        intIronBow = new int[256];
        intRainBow = new int[256];
        intGray = new int[256];
        intInvGray = new int[256];

        int Red;
        int Green;
        int Blue;
        for(int i = 0; i < 256; i++)
        {
            Red = Ironbow[i * 3];
            Green = Ironbow[i * 3 + 1];
            Blue = Ironbow[i * 3 + 2];
            intIronBow[i] = Red << 16 | Green << 8 | Blue;

            Red = Rainbow[i * 3];
            Green = Rainbow[i * 3 + 1];
            Blue = Rainbow[i * 3 + 2];
            intRainBow[i] = Red << 16 | Green << 8 | Blue;

            Red = Gray[i * 3];
            Green = Gray[i * 3 + 1];
            Blue = Gray[i * 3 + 2];
            intGray[i] = Red << 16 | Green << 8 | Blue;

            Red = Invgray[i * 3];
            Green = Invgray[i * 3 + 1];
            Blue = Invgray[i * 3 + 2];
            intInvGray[i] = Red << 16 | Green << 8 | Blue;
        }
    };


    public void SetImage (short[] Image, int width, int height){
        WIDTH = width;
        HEIGHT = height;

        // Convert TempArray to image
        min = 1<<15;
        max = 0;

        for (int i = 0; i < WIDTH*HEIGHT; i++)
        {
            TempArray[i] = Image[i];

            if(TempArray[i] > max){
                max = TempArray[i];
            }

            if(TempArray[i] < min){
                min = TempArray[i];
            }
        }

        // 14-bit to 8-bit conversion.
        for (int i = 0; i < WIDTH*HEIGHT; i++) {
            RawImage[i] = ( ((TempArray[i]) - min) * 255 / (max-min) );
        }

        int tempIndex = (HEIGHT-(WIDTH*540/960)/2)*WIDTH + WIDTH/2;

        float sumTemp = (TempArray[tempIndex] + TempArray[tempIndex+1] + TempArray[tempIndex+80] + TempArray[tempIndex+WIDTH+81]) >> 2;

        float A = a1*tempAUX + a2;
        float B = b1*tempAUX + b2;
        float C = c1*tempAUX*tempAUX + c2*tempAUX + c3;

        aimTemp = ( (-B + (float)Math.sqrt(B*B - 4*A*(C - sumTemp)) ) / (2*A) + 5.3f - 25.8f) * 0.9665f + 25.8f + shutterAuxDT;
        minTemp = ( (-B + (float)Math.sqrt(B*B - 4*A*(C - min)) ) / (2*A) + 5.3f - 25.8f) * 0.9665f + 25.8f + shutterAuxDT;
        maxTemp = ( (-B + (float)Math.sqrt(B*B - 4*A*(C - max)) ) / (2*A) + 5.3f - 25.8f) * 0.9665f + 25.8f + shutterAuxDT;


        /*
        aimTemp = (TempArray[tempIndex] + TempArray[tempIndex+1] + TempArray[tempIndex+80] + TempArray[tempIndex+81]) >> 2;
        //double Temp = ( ( -20.3097 + Math.sqrt(416.038 - 4*0.112626*(7680.3096 - aimTemp)) ) / (2*0.112626) );
        //double Temp = ( ( -17.585244 + Math.sqrt(309.2408 - 4*0.1080761*(7564.06074785 - aimTemp)) ) / (2*0.1080761) );
        Log.i("Temperature: ", Integer.toString(aimTemp));
        //Log.i("Temperature: ", String.valueOf(Temp));
        */

    };

    public float getTemp(int pixX, int pixY) {
        Log.i("aux", Float.toString(tempAUX));
        int tempIndex = pixY*80 + pixX;
        float sumTemp = (TempArray[tempIndex] + TempArray[tempIndex+1] + TempArray[tempIndex+80] + TempArray[tempIndex+81]) >> 2;

        float A = a1*tempAUX + a2;
        float B = b1*tempAUX + b2;
        float C = c1*tempAUX*tempAUX + c2*tempAUX + c3;

        aimTemp = ( (-B + (float)Math.sqrt(B*B - 4*A*(C - sumTemp)) ) / (2*A) + 5.3f - 25.8f) * 0.9665f + 25.8f;
        return aimTemp;
    }


    public int[] toRainbow (){
        for(int i = 0; i < WIDTH*HEIGHT; i++){
            ColorImage[i] = 0xff << 24 | intRainBow[RawImage[i]];
        }

        if(displayOrientation == Configuration.ORIENTATION_PORTRAIT) {
            int offsetSrc = 0;
            int offsetDst = 0;
            for(int j = 0; j < HEIGHT; j++){
                offsetDst = HEIGHT-1-j;
                for(int i = 0; i < WIDTH; i++){
                    revColorImage[offsetDst] = ColorImage[offsetSrc + i];
                    offsetDst += HEIGHT;
                }
                offsetSrc += WIDTH;
            }
            return revColorImage;
        }

        return ColorImage;
    };



    public int[] toIronbow (){
        for(int i = 0; i < WIDTH*HEIGHT; i++){
            ColorImage[i] = 0xff << 24 | intIronBow[RawImage[i]];
        }

        if(displayOrientation == Configuration.ORIENTATION_PORTRAIT) {
            int offsetSrc = 0;
            int offsetDst = 0;
            for(int j = 0; j < HEIGHT; j++){
                offsetDst = HEIGHT-1-j;
                for(int i = 0; i < WIDTH; i++){
                    TempArray[offsetDst] = ColorImage[offsetSrc + i];
                    offsetDst += HEIGHT;
                }
                offsetSrc += WIDTH;
            }
            return TempArray;
        }

        return ColorImage;
    };


    public int[] toGray (){
        for(int i = 0; i < WIDTH*HEIGHT; i++){
            ColorImage[i] = 0xff << 24 | intGray[RawImage[i]];
        }

        if(displayOrientation == Configuration.ORIENTATION_PORTRAIT) {
            int[] revColorImage = new int[WIDTH*HEIGHT];
            int offsetSrc = 0;
            int offsetDst = 0;
            for(int j = 0; j < HEIGHT; j++){
                offsetDst = HEIGHT-1-j;
                for(int i = 0; i < WIDTH; i++){
                    revColorImage[offsetDst] = ColorImage[offsetSrc + i];
                    offsetDst += HEIGHT;
                }
                offsetSrc += WIDTH;
            }
            return revColorImage;
        }

        return ColorImage;
    };


    public int[] toInvGray (){
        for(int i = 0; i < WIDTH*HEIGHT; i++){
            ColorImage[i] = 0xff << 24 | intInvGray[RawImage[i]];
        }

        if(displayOrientation == Configuration.ORIENTATION_PORTRAIT) {
            int offsetSrc = 0;
            int offsetDst = 0;
            for(int j = 0; j < HEIGHT; j++){
                offsetDst = HEIGHT-1-j;
                for(int i = 0; i < WIDTH; i++){
                    TempArray[offsetDst] = ColorImage[offsetSrc + i];
                    offsetDst += HEIGHT;
                }
                offsetSrc += WIDTH;
            }
            return TempArray;
        }

        return ColorImage;
    };




    // Lepton scaled Image to Gray with IronBow
    public int[] cutRangeImage () {
        float minF = (float) min;
        float maxF = (float) max;


        //  Translate selected temps to pixel ranges
        int minSelected = (int)( (maxF-minF)  * (minSelectedTemp-minTemp) / (maxTemp - minTemp) + minF );
        int maxSelected = (int)( (maxF-minF)  * (maxSelectedTemp-minTemp) / (maxTemp - minTemp) + minF );

        // deviation - this is
        int deviation = 15;
        int lowMinSelected = minSelected - deviation;
        int highMinSelected = minSelected;
        int lowMaxSelected = maxSelected;
        int highMaxSelected = maxSelected + deviation;

        for(int i = 0; i < WIDTH*HEIGHT; i++)
        {
            ColorImage[i] = 0;

            int Alpha = 0;
            int index = 0;
            if( (TempArray[i] >= lowMinSelected) && (TempArray[i] <= highMaxSelected) ) {
                if(highMaxSelected < max) {
                    index = ((( TempArray[i]) - lowMinSelected) * 255 / (highMaxSelected - lowMinSelected));
                }
                else
                if( (max - lowMinSelected) != 0 ) {
                    index = (((TempArray[i]) - lowMinSelected) * 255 / (max - lowMinSelected));
                }
            }

            if(TempArray[i] <= lowMinSelected) {
                Alpha = 0;
            }

            if( (TempArray[i] > lowMinSelected) &&  (TempArray[i] <= highMinSelected)) {
                Alpha = ( ( TempArray[i]) - lowMinSelected) * 255 / (highMinSelected - lowMinSelected);
            }

            if( (TempArray[i] > highMinSelected) && (TempArray[i] <= lowMaxSelected) ){
                Alpha = 255;
            }

            if( (TempArray[i] > lowMaxSelected) &&  (TempArray[i] <= highMaxSelected) ) {
                Alpha = 255 - ( ( ( TempArray[i]) - lowMinSelected) * 255 / (highMinSelected - lowMinSelected));
            }

            if( TempArray[i] > highMaxSelected ){
                Alpha = 0;
            }

            //ColorImage[i] = Alpha << 24 | intIronBow[index];
            ColorImage[i] = Alpha << 24 | 0xF53333;
        }

        return ColorImage;
    };

    public void setShutterAuxDT (float dT) {
        shutterAuxDT = dT;
    }

    public float getHorizontalViewAngle() {
        return HORIZONTAL_ANGLE;
    }

    public float getVerticalViewAngle() {
        return VERTICAL_ANGLE;
    }

    final private char[] WhiteBlue = {255, 255, 255, 254, 254, 255, 253, 253, 255, 252, 252, 255, 251, 251, 255, 250, 250, 255, 249, 249, 255, 248, 248, 255, 247, 247, 255, 246, 246, 255, 245, 245, 255, 244, 244, 255, 243, 243, 255, 242, 242, 255, 241, 241, 255, 240, 240, 255, 239, 239, 255, 238, 238, 255, 237, 237, 255, 236, 236, 255, 235, 235, 255, 234, 234, 255, 233, 233, 255, 232, 232, 255, 231, 231, 255, 230, 230, 255, 229, 229, 255, 228, 228, 255, 227, 227, 255, 226, 226, 255, 225, 225, 255, 224, 224, 255, 223, 223, 255, 222, 222, 255, 221, 221, 255, 220, 220, 255, 219, 219, 255, 218, 218, 255, 217, 217, 255, 216, 216, 255, 215, 215, 255, 214, 214, 255, 213, 213, 255, 212, 212, 255, 211, 211, 255, 210, 210, 255, 209, 209, 255, 208, 208, 255, 207, 207, 255, 206, 206, 255, 205, 205, 255, 204, 204, 255, 203, 203, 255, 202, 202, 255, 201, 201, 255, 200, 200, 255, 199, 199, 255, 198, 198, 255, 197, 197, 255, 196, 196, 255, 195, 195, 255, 194, 194, 255, 193, 193, 255, 192, 192, 255, 191, 191, 255, 190, 190, 255, 189, 189, 255, 188, 188, 255, 187, 187, 255, 186, 186, 255, 185, 185, 255, 184, 184, 255, 183, 183, 255, 182, 182, 255, 181, 181, 255, 180, 180, 255, 179, 179, 255, 178, 178, 255, 177, 177, 255, 176, 176, 255, 175, 175, 255, 174, 174, 255, 173, 173, 255, 172, 172, 255, 171, 171, 255, 170, 170, 255, 169, 169, 255, 168, 168, 255, 167, 167, 255, 166, 166, 255, 165, 165, 255, 164, 164, 255, 163, 163, 255, 162, 162, 255, 161, 161, 255, 160, 160, 255, 159, 159, 255, 158, 158, 255, 157, 157, 255, 156, 156, 255, 155, 155, 255, 154, 154, 255, 153, 153, 255, 152, 152, 255, 151, 151, 255, 150, 150, 255, 149, 149, 255, 148, 148, 255, 147, 147, 255, 146, 146, 255, 145, 145, 255, 144, 144, 255, 143, 143, 255, 142, 142, 255, 141, 141, 255, 140, 140, 255, 139, 139, 255, 138, 138, 255, 137, 137, 255, 136, 136, 255, 135, 135, 255, 134, 134, 255, 133, 133, 255, 132, 132, 255, 131, 131, 255, 130, 130, 255, 129, 129, 255, 128, 128, 255, 127, 127, 255, 126, 126, 255, 125, 125, 255, 124, 124, 255, 123, 123, 255, 122, 122, 255, 121, 121, 255, 120, 120, 255, 119, 119, 255, 118, 118, 255, 117, 117, 255, 116, 116, 255, 115, 115, 255, 114, 114, 255, 113, 113, 255, 112, 112, 255, 111, 111, 255, 110, 110, 255, 109, 109, 255, 108, 108, 255, 107, 107, 255, 106, 106, 255, 105, 105, 255, 104, 104, 255, 103, 103, 255, 102, 102, 255, 101, 101, 255, 100, 100, 255, 99, 99, 255, 98, 98, 255, 97, 97, 255, 96, 96, 255, 95, 95, 255, 94, 94, 255, 93, 93, 255, 92, 92, 255, 91, 91, 255, 90, 90, 255, 89, 89, 255, 88, 88, 255, 87, 87, 255, 86, 86, 255, 85, 85, 255, 84, 84, 255, 83, 83, 255, 82, 82, 255, 81, 81, 255, 80, 80, 255, 79, 79, 255, 78, 78, 255, 77, 77, 255, 76, 76, 255, 75, 75, 255, 74, 74, 255, 73, 73, 255, 72, 72, 255, 71, 71, 255, 70, 70, 255, 69, 69, 255, 68, 68, 255, 67, 67, 255, 66, 66, 255, 65, 65, 255, 64, 64, 255, 63, 63, 255, 62, 62, 255, 61, 61, 255, 60, 60, 255, 59, 59, 255, 58, 58, 255, 57, 57, 255, 56, 56, 255, 55, 55, 255, 54, 54, 255, 53, 53, 255, 52, 52, 255, 51, 51, 255, 50, 50, 255, 49, 49, 255, 48, 48, 255, 47, 47, 255, 46, 46, 255, 45, 45, 255, 44, 44, 255, 43, 43, 255, 42, 42, 255, 41, 41, 255, 40, 40, 255, 39, 39, 255, 38, 38, 255, 37, 37, 255, 36, 36, 255, 35, 35, 255, 34, 34, 255, 33, 33, 255, 32, 32, 255, 31, 31, 255, 30, 30, 255, 29, 29, 255, 28, 28, 255, 27, 27, 255, 26, 26, 255, 25, 25, 255, 24, 24, 255, 23, 23, 255, 22, 22, 255, 21, 21, 255, 20, 20, 255, 19, 19, 255, 18, 18, 255, 17, 17, 255, 16, 16, 255, 15, 15, 255, 14, 14, 255, 13, 13, 255, 12, 12, 255, 11, 11, 255, 10, 10, 255, 9, 9, 255, 8, 8, 255, 7, 7, 255, 6, 6, 255, 5, 5, 255, 4, 4, 255, 3, 3, 255, 2, 2, 255, 1, 1, 255, 0, 0, 255};

    final private char[] Rainbow = {    1, 3, 74,
                                        0, 3, 74,
                                        0, 3, 75,
                                        0, 3, 75,
                                        0, 3, 76,
                                        0, 3, 76,
                                        0, 3, 77,
                                        0, 3, 79,
                                        0, 3, 82,
                                        0, 5, 85,
                                        0, 7, 88,
                                        0, 10, 91,
                                        0, 14, 94,
                                        0, 19, 98,
                                        0, 22, 100,
                                        0, 25, 103,
                                        0, 28, 106,
                                        0, 32, 109,
                                        0, 35, 112,
                                        0, 38, 116,
                                        0, 40, 119,
                                        0, 42, 123,
                                        0, 45, 128,
                                        0, 49, 133,
                                        0, 50, 134,
                                        0, 51, 136,
                                        0, 52, 137,
                                        0, 53, 139,
                                        0, 54, 142,
                                        0, 55, 144,
                                        0, 56, 145,
                                        0, 58, 149,
                                        0, 61, 154,
                                        0, 63, 156,
                                        0, 65, 159,
                                        0, 66, 161,
                                        0, 68, 164,
                                        0, 69, 167,
                                        0, 71, 170,
                                        0, 73, 174,
                                        0, 75, 179,
                                        0, 76, 181,
                                        0, 78, 184,
                                        0, 79, 187,
                                        0, 80, 188,
                                        0, 81, 190,
                                        0, 84, 194,
                                        0, 87, 198,
                                        0, 88, 200,
                                        0, 90, 203,
                                        0, 92, 205,
                                        0, 94, 207,
                                        0, 94, 208,
                                        0, 95, 209,
                                        0, 96, 210,
                                        0, 97, 211,
                                        0, 99, 214,
                                        0, 102, 217,
                                        0, 103, 218,
                                        0, 104, 219,
                                        0, 105, 220,
                                        0, 107, 221,
                                        0, 109, 223,
                                        0, 111, 223,
                                        0, 113, 223,
                                        0, 115, 222,
                                        0, 117, 221,
                                        0, 118, 220,
                                        1, 120, 219,
                                        1, 122, 217,
                                        2, 124, 216,
                                        2, 126, 214,
                                        3, 129, 212,
                                        3, 131, 207,
                                        4, 132, 205,
                                        4, 133, 202,
                                        4, 134, 197,
                                        5, 136, 192,
                                        6, 138, 185,
                                        7, 141, 178,
                                        8, 142, 172,
                                        10, 144, 166,
                                        10, 144, 162,
                                        11, 145, 158,
                                        12, 146, 153,
                                        13, 147, 149,
                                        15, 149, 140,
                                        17, 151, 132,
                                        22, 153, 120,
                                        25, 154, 115,
                                        28, 156, 109,
                                        34, 158, 101,
                                        40, 160, 94,
                                        45, 162, 86,
                                        51, 164, 79,
                                        59, 167, 69,
                                        67, 171, 60,
                                        72, 173, 54,
                                        78, 175, 48,
                                        83, 177, 43,
                                        89, 179, 39,
                                        93, 181, 35,
                                        98, 183, 31,
                                        105, 185, 26,
                                        109, 187, 23,
                                        113, 188, 21,
                                        118, 189, 19,
                                        123, 191, 17,
                                        128, 193, 14,
                                        134, 195, 12,
                                        138, 196, 10,
                                        142, 197, 8,
                                        146, 198, 6,
                                        151, 200, 5,
                                        155, 201, 4,
                                        160, 203, 3,
                                        164, 204, 2,
                                        169, 205, 2,
                                        173, 206, 1,
                                        175, 207, 1,
                                        178, 207, 1,
                                        184, 208, 0,
                                        190, 210, 0,
                                        193, 211, 0,
                                        196, 212, 0,
                                        199, 212, 0,
                                        202, 213, 1,
                                        207, 214, 2,
                                        212, 215, 3,
                                        215, 214, 3,
                                        218, 214, 3,
                                        220, 213, 3,
                                        222, 213, 4,
                                        224, 212, 4,
                                        225, 212, 5,
                                        226, 212, 5,
                                        229, 211, 5,
                                        232, 211, 6,
                                        232, 211, 6,
                                        233, 211, 6,
                                        234, 210, 6,
                                        235, 210, 7,
                                        236, 209, 7,
                                        237, 208, 8,
                                        239, 206, 8,
                                        241, 204, 9,
                                        242, 203, 9,
                                        244, 202, 10,
                                        244, 201, 10,
                                        245, 200, 10,
                                        245, 199, 11,
                                        246, 198, 11,
                                        247, 197, 12,
                                        248, 194, 13,
                                        249, 191, 14,
                                        250, 189, 14,
                                        251, 187, 15,
                                        251, 185, 16,
                                        252, 183, 17,
                                        252, 178, 18,
                                        253, 174, 19,
                                        253, 171, 19,
                                        254, 168, 20,
                                        254, 165, 21,
                                        254, 164, 21,
                                        255, 163, 22,
                                        255, 161, 22,
                                        255, 159, 23,
                                        255, 157, 23,
                                        255, 155, 24,
                                        255, 149, 25,
                                        255, 143, 27,
                                        255, 139, 28,
                                        255, 135, 30,
                                        255, 131, 31,
                                        255, 127, 32,
                                        255, 118, 34,
                                        255, 110, 36,
                                        255, 104, 37,
                                        255, 101, 38,
                                        255, 99, 39,
                                        255, 93, 40,
                                        255, 88, 42,
                                        254, 82, 43,
                                        254, 77, 45,
                                        254, 69, 47,
                                        254, 62, 49,
                                        253, 57, 50,
                                        253, 53, 52,
                                        252, 49, 53,
                                        252, 45, 55,
                                        251, 39, 57,
                                        251, 33, 59,
                                        251, 32, 60,
                                        251, 31, 60,
                                        251, 30, 61,
                                        251, 29, 61,
                                        251, 28, 62,
                                        250, 27, 63,
                                        250, 27, 65,
                                        249, 26, 66,
                                        249, 26, 68,
                                        248, 25, 70,
                                        248, 24, 73,
                                        247, 24, 75,
                                        247, 25, 77,
                                        247, 25, 79,
                                        247, 26, 81,
                                        247, 32, 83,
                                        247, 35, 85,
                                        247, 38, 86,
                                        247, 42, 88,
                                        247, 46, 90,
                                        247, 50, 92,
                                        248, 55, 94,
                                        248, 59, 96,
                                        248, 64, 98,
                                        248, 72, 101,
                                        249, 81, 104,
                                        249, 87, 106,
                                        250, 93, 108,
                                        250, 95, 109,
                                        250, 98, 110,
                                        250, 100, 111,
                                        251, 101, 112,
                                        251, 102, 113,
                                        251, 109, 117,
                                        252, 116, 121,
                                        252, 121, 123,
                                        253, 126, 126,
                                        253, 130, 128,
                                        254, 135, 131,
                                        254, 139, 133,
                                        254, 144, 136,
                                        254, 151, 140,
                                        255, 158, 144,
                                        255, 163, 146,
                                        255, 168, 149,
                                        255, 173, 152,
                                        255, 176, 153,
                                        255, 178, 155,
                                        255, 184, 160,
                                        255, 191, 165,
                                        255, 195, 168,
                                        255, 199, 172,
                                        255, 203, 175,
                                        255, 207, 179,
                                        255, 211, 182,
                                        255, 216, 185,
                                        255, 218, 190,
                                        255, 220, 196,
                                        255, 222, 200,
                                        255, 225, 202,
                                        255, 227, 204,
                                        255, 230, 206,
                                        255, 233, 208   };


    final private char[] Ironbow = {    0,0,0,
                                        0,0,16,
                                        0,0,33,
                                        0,0,42,
                                        0,0,49,
                                        0,0,56,
                                        0,0,63,
                                        0,0,70,
                                        0,0,77,
                                        0,0,83,
                                        1,0,87,
                                        2,0,91,
                                        3,0,95,
                                        4,0,99,
                                        5,0,103,
                                        7,0,106,
                                        9,0,110,
                                        11,0,115,
                                        12,0,116,
                                        13,0,118,
                                        16,0,120,
                                        19,0,122,
                                        22,0,124,
                                        25,0,127,
                                        28,0,129,
                                        31,0,131,
                                        34,0,133,
                                        38,0,135,
                                        42,0,137,
                                        45,0,138,
                                        48,0,140,
                                        52,0,141,
                                        55,0,143,
                                        58,0,144,
                                        61,0,146,
                                        63,0,147,
                                        65,0,148,
                                        68,0,149,
                                        71,0,149,
                                        74,0,150,
                                        76,0,150,
                                        79,0,151,
                                        82,0,151,
                                        85,0,152,
                                        88,0,152,
                                        92,0,153,
                                        94,0,154,
                                        97,0,155,
                                        101,0,155,
                                        104,0,155,
                                        107,0,155,
                                        110,0,156,
                                        112,0,156,
                                        114,0,156,
                                        117,0,157,
                                        121,0,157,
                                        124,0,157,
                                        126,0,157,
                                        129,0,157,
                                        132,0,157,
                                        135,0,157,
                                        137,0,157,
                                        140,0,156,
                                        143,0,156,
                                        146,0,155,
                                        149,0,155,
                                        152,0,155,
                                        154,0,155,
                                        157,0,155,
                                        159,0,155,
                                        161,0,155,
                                        164,0,154,
                                        166,0,154,
                                        168,0,153,
                                        170,0,153,
                                        172,0,152,
                                        174,0,152,
                                        175,1,151,
                                        177,1,151,
                                        178,1,150,
                                        180,1,149,
                                        182,2,149,
                                        183,3,149,
                                        185,4,148,
                                        186,4,147,
                                        188,5,147,
                                        189,5,146,
                                        190,5,146,
                                        191,6,145,
                                        192,7,144,
                                        193,9,143,
                                        194,10,142,
                                        195,11,141,
                                        197,12,139,
                                        198,13,138,
                                        200,15,136,
                                        201,17,134,
                                        202,18,133,
                                        203,20,131,
                                        204,21,129,
                                        206,23,126,
                                        207,24,123,
                                        208,26,121,
                                        208,27,118,
                                        209,28,116,
                                        210,30,113,
                                        211,32,111,
                                        212,34,108,
                                        213,36,104,
                                        214,38,101,
                                        216,40,98,
                                        217,42,95,
                                        218,44,91,
                                        219,46,87,
                                        220,47,81,
                                        221,49,76,
                                        222,51,70,
                                        223,53,65,
                                        223,54,59,
                                        224,56,54,
                                        224,57,48,
                                        225,59,42,
                                        226,61,37,
                                        227,63,31,
                                        228,65,28,
                                        228,67,25,
                                        229,69,23,
                                        230,71,21,
                                        231,72,19,
                                        231,74,17,
                                        232,76,15,
                                        233,77,13,
                                        234,78,11,
                                        234,80,10,
                                        235,82,9,
                                        235,84,8,
                                        236,86,8,
                                        236,87,7,
                                        236,89,7,
                                        237,91,6,
                                        237,92,5,
                                        238,94,4,
                                        238,95,4,
                                        239,97,3,
                                        239,99,3,
                                        240,100,3,
                                        240,102,3,
                                        241,103,2,
                                        241,104,2,
                                        241,106,1,
                                        241,107,1,
                                        242,109,1,
                                        242,111,1,
                                        243,113,1,
                                        243,114,0,
                                        243,115,0,
                                        244,117,0,
                                        244,119,0,
                                        244,121,0,
                                        244,124,0,
                                        245,126,0,
                                        245,128,0,
                                        246,129,0,
                                        246,131,0,
                                        247,133,0,
                                        247,134,0,
                                        248,136,0,
                                        248,137,0,
                                        248,139,0,
                                        248,140,0,
                                        249,142,0,
                                        249,143,0,
                                        249,144,0,
                                        249,146,0,
                                        249,148,0,
                                        250,150,0,
                                        250,153,0,
                                        251,155,0,
                                        251,157,0,
                                        252,159,0,
                                        252,161,0,
                                        253,163,0,
                                        253,166,0,
                                        253,168,0,
                                        253,170,0,
                                        253,172,0,
                                        253,174,0,
                                        254,176,0,
                                        254,177,0,
                                        254,178,0,
                                        254,181,0,
                                        254,183,0,
                                        254,185,0,
                                        254,186,0,
                                        254,188,0,
                                        254,190,0,
                                        254,191,0,
                                        254,193,0,
                                        254,195,0,
                                        254,197,0,
                                        254,199,0,
                                        254,200,0,
                                        254,202,1,
                                        254,203,1,
                                        254,205,2,
                                        254,206,3,
                                        254,207,4,
                                        254,209,6,
                                        254,211,8,
                                        254,213,10,
                                        254,215,11,
                                        254,216,12,
                                        254,218,14,
                                        255,219,16,
                                        255,220,20,
                                        255,221,24,
                                        255,222,28,
                                        255,224,32,
                                        255,225,36,
                                        255,227,39,
                                        255,228,44,
                                        255,229,50,
                                        255,230,56,
                                        255,231,62,
                                        255,233,67,
                                        255,234,73,
                                        255,236,79,
                                        255,237,85,
                                        255,238,92,
                                        255,238,98,
                                        255,239,105,
                                        255,240,111,
                                        255,241,119,
                                        255,241,127,
                                        255,242,135,
                                        255,243,142,
                                        255,244,149,
                                        255,244,156,
                                        255,245,164,
                                        255,245,171,
                                        255,246,178,
                                        255,247,184,
                                        255,247,190,
                                        255,248,195,
                                        255,248,201,
                                        255,249,206,
                                        255,250,212,
                                        255,251,218,
                                        255,252,224,
                                        255,253,229,
                                        255,253,235,
                                        255,254,240,
                                        255,254,244,
                                        255,255,249,
                                        255,255,252,
                                        255,255,255     };

    final private char[] Gray =    {    0, 0, 0,
                                        1, 1, 1,
                                        2, 2, 2,
                                        3, 3, 3,
                                        4, 4, 4,
                                        5, 5, 5,
                                        6, 6, 6,
                                        7, 7, 7,
                                        8, 8, 8,
                                        9, 9, 9,
                                        10, 10, 10,
                                        11, 11, 11,
                                        12, 12, 12,
                                        13, 13, 13,
                                        14, 14, 14,
                                        15, 15, 15,
                                        16, 16, 16,
                                        17, 17, 17,
                                        18, 18, 18,
                                        19, 19, 19,
                                        20, 20, 20,
                                        21, 21, 21,
                                        22, 22, 22,
                                        23, 23, 23,
                                        24, 24, 24,
                                        25, 25, 25,
                                        26, 26, 26,
                                        27, 27, 27,
                                        28, 28, 28,
                                        29, 29, 29,
                                        30, 30, 30,
                                        31, 31, 31,
                                        32, 32, 32,
                                        33, 33, 33,
                                        34, 34, 34,
                                        35, 35, 35,
                                        36, 36, 36,
                                        37, 37, 37,
                                        38, 38, 38,
                                        39, 39, 39,
                                        40, 40, 40,
                                        41, 41, 41,
                                        42, 42, 42,
                                        43, 43, 43,
                                        44, 44, 44,
                                        45, 45, 45,
                                        46, 46, 46,
                                        47, 47, 47,
                                        48, 48, 48,
                                        49, 49, 49,
                                        50, 50, 50,
                                        51, 51, 51,
                                        52, 52, 52,
                                        53, 53, 53,
                                        54, 54, 54,
                                        55, 55, 55,
                                        56, 56, 56,
                                        57, 57, 57,
                                        58, 58, 58,
                                        59, 59, 59,
                                        60, 60, 60,
                                        61, 61, 61,
                                        62, 62, 62,
                                        63, 63, 63,
                                        64, 64, 64,
                                        65, 65, 65,
                                        66, 66, 66,
                                        67, 67, 67,
                                        68, 68, 68,
                                        69, 69, 69,
                                        70, 70, 70,
                                        71, 71, 71,
                                        72, 72, 72,
                                        73, 73, 73,
                                        74, 74, 74,
                                        75, 75, 75,
                                        76, 76, 76,
                                        77, 77, 77,
                                        78, 78, 78,
                                        79, 79, 79,
                                        80, 80, 80,
                                        81, 81, 81,
                                        82, 82, 82,
                                        83, 83, 83,
                                        84, 84, 84,
                                        85, 85, 85,
                                        86, 86, 86,
                                        87, 87, 87,
                                        88, 88, 88,
                                        89, 89, 89,
                                        90, 90, 90,
                                        91, 91, 91,
                                        92, 92, 92,
                                        93, 93, 93,
                                        94, 94, 94,
                                        95, 95, 95,
                                        96, 96, 96,
                                        97, 97, 97,
                                        98, 98, 98,
                                        99, 99, 99,
                                        100, 100, 100,
                                        101, 101, 101,
                                        102, 102, 102,
                                        103, 103, 103,
                                        104, 104, 104,
                                        105, 105, 105,
                                        106, 106, 106,
                                        107, 107, 107,
                                        108, 108, 108,
                                        109, 109, 109,
                                        110, 110, 110,
                                        111, 111, 111,
                                        112, 112, 112,
                                        113, 113, 113,
                                        114, 114, 114,
                                        115, 115, 115,
                                        116, 116, 116,
                                        117, 117, 117,
                                        118, 118, 118,
                                        119, 119, 119,
                                        120, 120, 120,
                                        121, 121, 121,
                                        122, 122, 122,
                                        123, 123, 123,
                                        124, 124, 124,
                                        125, 125, 125,
                                        126, 126, 126,
                                        127, 127, 127,
                                        128, 128, 128,
                                        129, 129, 129,
                                        130, 130, 130,
                                        131, 131, 131,
                                        132, 132, 132,
                                        133, 133, 133,
                                        134, 134, 134,
                                        135, 135, 135,
                                        136, 136, 136,
                                        137, 137, 137,
                                        138, 138, 138,
                                        139, 139, 139,
                                        140, 140, 140,
                                        141, 141, 141,
                                        142, 142, 142,
                                        143, 143, 143,
                                        144, 144, 144,
                                        145, 145, 145,
                                        146, 146, 146,
                                        147, 147, 147,
                                        148, 148, 148,
                                        149, 149, 149,
                                        150, 150, 150,
                                        151, 151, 151,
                                        152, 152, 152,
                                        153, 153, 153,
                                        154, 154, 154,
                                        155, 155, 155,
                                        156, 156, 156,
                                        157, 157, 157,
                                        158, 158, 158,
                                        159, 159, 159,
                                        160, 160, 160,
                                        161, 161, 161,
                                        162, 162, 162,
                                        163, 163, 163,
                                        164, 164, 164,
                                        165, 165, 165,
                                        166, 166, 166,
                                        167, 167, 167,
                                        168, 168, 168,
                                        169, 169, 169,
                                        170, 170, 170,
                                        171, 171, 171,
                                        172, 172, 172,
                                        173, 173, 173,
                                        174, 174, 174,
                                        175, 175, 175,
                                        176, 176, 176,
                                        177, 177, 177,
                                        178, 178, 178,
                                        179, 179, 179,
                                        180, 180, 180,
                                        181, 181, 181,
                                        182, 182, 182,
                                        183, 183, 183,
                                        184, 184, 184,
                                        185, 185, 185,
                                        186, 186, 186,
                                        187, 187, 187,
                                        188, 188, 188,
                                        189, 189, 189,
                                        190, 190, 190,
                                        191, 191, 191,
                                        192, 192, 192,
                                        193, 193, 193,
                                        194, 194, 194,
                                        195, 195, 195,
                                        196, 196, 196,
                                        197, 197, 197,
                                        198, 198, 198,
                                        199, 199, 199,
                                        200, 200, 200,
                                        201, 201, 201,
                                        202, 202, 202,
                                        203, 203, 203,
                                        204, 204, 204,
                                        205, 205, 205,
                                        206, 206, 206,
                                        207, 207, 207,
                                        208, 208, 208,
                                        209, 209, 209,
                                        210, 210, 210,
                                        211, 211, 211,
                                        212, 212, 212,
                                        213, 213, 213,
                                        214, 214, 214,
                                        215, 215, 215,
                                        216, 216, 216,
                                        217, 217, 217,
                                        218, 218, 218,
                                        219, 219, 219,
                                        220, 220, 220,
                                        221, 221, 221,
                                        222, 222, 222,
                                        223, 223, 223,
                                        224, 224, 224,
                                        225, 225, 225,
                                        226, 226, 226,
                                        227, 227, 227,
                                        228, 228, 228,
                                        229, 229, 229,
                                        230, 230, 230,
                                        231, 231, 231,
                                        232, 232, 232,
                                        233, 233, 233,
                                        234, 234, 234,
                                        235, 235, 235,
                                        236, 236, 236,
                                        237, 237, 237,
                                        238, 238, 238,
                                        239, 239, 239,
                                        240, 240, 240,
                                        241, 241, 241,
                                        242, 242, 242,
                                        243, 243, 243,
                                        244, 244, 244,
                                        245, 245, 245,
                                        246, 246, 246,
                                        247, 247, 247,
                                        248, 248, 248,
                                        249, 249, 249,
                                        250, 250, 250,
                                        251, 251, 251,
                                        252, 252, 252,
                                        253, 253, 253,
                                        254, 254, 254,
                                        255, 255, 255   };


    final private char[] Invgray =  {   255, 255, 255,
                                        254, 254, 254,
                                        253, 253, 253,
                                        252, 252, 252,
                                        251, 251, 251,
                                        250, 250, 250,
                                        249, 249, 249,
                                        248, 248, 248,
                                        247, 247, 247,
                                        246, 246, 246,
                                        245, 245, 245,
                                        244, 244, 244,
                                        243, 243, 243,
                                        242, 242, 242,
                                        241, 241, 241,
                                        240, 240, 240,
                                        239, 239, 239,
                                        238, 238, 238,
                                        237, 237, 237,
                                        236, 236, 236,
                                        235, 235, 235,
                                        234, 234, 234,
                                        233, 233, 233,
                                        232, 232, 232,
                                        231, 231, 231,
                                        230, 230, 230,
                                        229, 229, 229,
                                        228, 228, 228,
                                        227, 227, 227,
                                        226, 226, 226,
                                        225, 225, 225,
                                        224, 224, 224,
                                        223, 223, 223,
                                        222, 222, 222,
                                        221, 221, 221,
                                        220, 220, 220,
                                        219, 219, 219,
                                        218, 218, 218,
                                        217, 217, 217,
                                        216, 216, 216,
                                        215, 215, 215,
                                        214, 214, 214,
                                        213, 213, 213,
                                        212, 212, 212,
                                        211, 211, 211,
                                        210, 210, 210,
                                        209, 209, 209,
                                        208, 208, 208,
                                        207, 207, 207,
                                        206, 206, 206,
                                        205, 205, 205,
                                        204, 204, 204,
                                        203, 203, 203,
                                        202, 202, 202,
                                        201, 201, 201,
                                        200, 200, 200,
                                        199, 199, 199,
                                        198, 198, 198,
                                        197, 197, 197,
                                        196, 196, 196,
                                        195, 195, 195,
                                        194, 194, 194,
                                        193, 193, 193,
                                        192, 192, 192,
                                        191, 191, 191,
                                        190, 190, 190,
                                        189, 189, 189,
                                        188, 188, 188,
                                        187, 187, 187,
                                        186, 186, 186,
                                        185, 185, 185,
                                        184, 184, 184,
                                        183, 183, 183,
                                        182, 182, 182,
                                        181, 181, 181,
                                        180, 180, 180,
                                        179, 179, 179,
                                        178, 178, 178,
                                        177, 177, 177,
                                        176, 176, 176,
                                        175, 175, 175,
                                        174, 174, 174,
                                        173, 173, 173,
                                        172, 172, 172,
                                        171, 171, 171,
                                        170, 170, 170,
                                        169, 169, 169,
                                        168, 168, 168,
                                        167, 167, 167,
                                        166, 166, 166,
                                        165, 165, 165,
                                        164, 164, 164,
                                        163, 163, 163,
                                        162, 162, 162,
                                        161, 161, 161,
                                        160, 160, 160,
                                        159, 159, 159,
                                        158, 158, 158,
                                        157, 157, 157,
                                        156, 156, 156,
                                        155, 155, 155,
                                        154, 154, 154,
                                        153, 153, 153,
                                        152, 152, 152,
                                        151, 151, 151,
                                        150, 150, 150,
                                        149, 149, 149,
                                        148, 148, 148,
                                        147, 147, 147,
                                        146, 146, 146,
                                        145, 145, 145,
                                        144, 144, 144,
                                        143, 143, 143,
                                        142, 142, 142,
                                        141, 141, 141,
                                        140, 140, 140,
                                        139, 139, 139,
                                        138, 138, 138,
                                        137, 137, 137,
                                        136, 136, 136,
                                        135, 135, 135,
                                        134, 134, 134,
                                        133, 133, 133,
                                        132, 132, 132,
                                        131, 131, 131,
                                        130, 130, 130,
                                        129, 129, 129,
                                        128, 128, 128,
                                        127, 127, 127,
                                        126, 126, 126,
                                        125, 125, 125,
                                        124, 124, 124,
                                        123, 123, 123,
                                        122, 122, 122,
                                        121, 121, 121,
                                        120, 120, 120,
                                        119, 119, 119,
                                        118, 118, 118,
                                        117, 117, 117,
                                        116, 116, 116,
                                        115, 115, 115,
                                        114, 114, 114,
                                        113, 113, 113,
                                        112, 112, 112,
                                        111, 111, 111,
                                        110, 110, 110,
                                        109, 109, 109,
                                        108, 108, 108,
                                        107, 107, 107,
                                        106, 106, 106,
                                        105, 105, 105,
                                        104, 104, 104,
                                        103, 103, 103,
                                        102, 102, 102,
                                        101, 101, 101,
                                        100, 100, 100,
                                        99, 99, 99,
                                        98, 98, 98,
                                        97, 97, 97,
                                        96, 96, 96,
                                        95, 95, 95,
                                        94, 94, 94,
                                        93, 93, 93,
                                        92, 92, 92,
                                        91, 91, 91,
                                        90, 90, 90,
                                        89, 89, 89,
                                        88, 88, 88,
                                        87, 87, 87,
                                        86, 86, 86,
                                        85, 85, 85,
                                        84, 84, 84,
                                        83, 83, 83,
                                        82, 82, 82,
                                        81, 81, 81,
                                        80, 80, 80,
                                        79, 79, 79,
                                        78, 78, 78,
                                        77, 77, 77,
                                        76, 76, 76,
                                        75, 75, 75,
                                        74, 74, 74,
                                        73, 73, 73,
                                        72, 72, 72,
                                        71, 71, 71,
                                        70, 70, 70,
                                        69, 69, 69,
                                        68, 68, 68,
                                        67, 67, 67,
                                        66, 66, 66,
                                        65, 65, 65,
                                        64, 64, 64,
                                        63, 63, 63,
                                        62, 62, 62,
                                        61, 61, 61,
                                        60, 60, 60,
                                        59, 59, 59,
                                        58, 58, 58,
                                        57, 57, 57,
                                        56, 56, 56,
                                        55, 55, 55,
                                        54, 54, 54,
                                        53, 53, 53,
                                        52, 52, 52,
                                        51, 51, 51,
                                        50, 50, 50,
                                        49, 49, 49,
                                        48, 48, 48,
                                        47, 47, 47,
                                        46, 46, 46,
                                        45, 45, 45,
                                        44, 44, 44,
                                        43, 43, 43,
                                        42, 42, 42,
                                        41, 41, 41,
                                        40, 40, 40,
                                        39, 39, 39,
                                        38, 38, 38,
                                        37, 37, 37,
                                        36, 36, 36,
                                        35, 35, 35,
                                        34, 34, 34,
                                        33, 33, 33,
                                        32, 32, 32,
                                        31, 31, 31,
                                        30, 30, 30,
                                        29, 29, 29,
                                        28, 28, 28,
                                        27, 27, 27,
                                        26, 26, 26,
                                        25, 25, 25,
                                        24, 24, 24,
                                        23, 23, 23,
                                        22, 22, 22,
                                        21, 21, 21,
                                        20, 20, 20,
                                        19, 19, 19,
                                        18, 18, 18,
                                        17, 17, 17,
                                        16, 16, 16,
                                        15, 15, 15,
                                        14, 14, 14,
                                        13, 13, 13,
                                        12, 12, 12,
                                        11, 11, 11,
                                        10, 10, 10,
                                        9, 9, 9,
                                        8, 8, 8,
                                        7, 7, 7,
                                        6, 6, 6,
                                        5, 5, 5,
                                        4, 4, 4,
                                        3, 3, 3,
                                        2, 2, 2,
                                        1, 1, 1,
                                        0, 0, 0     };

}
