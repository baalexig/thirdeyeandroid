package by.axonim.te_phone;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.github.hiteshsondhi88.libffmpeg.myFFmpeg;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class MainActivity extends ActionBarActivity implements SurfaceHolder.Callback {

    CompassView compassView;
    //The "x" and "y" position of the "Show Button" on screen.
    Point p;


    RelativeLayout mainRelativeLayout;
    View subMenuView;
    View menuView;
    View recView;
    SurfaceView surfaceView;
    SurfaceView compassSurface;
    ViewGroup parentViewGroup;
    TextView tempTextView;
    TextView textRecTime;
    TextView clockTextView;
    TextView compassTextView;
    ImageView tempImageView;
    ImageView aimImageView;
    ListView wifiListView;
    Button takePictureButton;
    Button menuButton;


    byte[] cc3200UpdateFile;

    private Camera camera;
    private SurfaceView preview;
    private CameraProcessing cameraProcessing;



    private ToggleButton toggleRec;
    private ToggleButton toggleMode;
    private ImageView closeMenuBtn;


    // State variables
    volatile boolean isRecording;
    volatile boolean isPhotoMode;
    volatile boolean isCompassCalibrate;
    boolean isSaveInstanse;
    boolean isClockVisible = true;
    boolean isCompassVisible = true;


    private DrawThread drawThread;
    SurfaceView mSurface;


    Thread serverFrameThread = null;
    ClientCommandThread clientCommandThread = null;
    UpdateUiThread updateUiThread = null;

    myFFmpeg ffmpeg;
    public Lepton lepton;
    private Compass compass = new Compass();
    Bitmap outBitmap;
    float deltaT;


    private int displayOrientation;
    public int widthSurface;
    public int heightSurface;
    private int cameraXOffset;
    private int cameraYOffset;
    private float cameraScale = 1.0f;


    float azimuthInDegress;
    int pallette = 0;
    int recordVideoFmt = 0;


    long recordVideoStartTimeMillis;
    long recordVideoLastTime;


    WifiManager wifiManager;
    WifiListAdapter wifiListAdapter;
    ArrayList<WifiListItem> TE_wifis = new ArrayList<WifiListItem>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        /*
        TextView textView = (TextView) findViewById(R.id.compassTextView);
        textView.setTypeface(Typeface.create("sans-serif-thin", Typeface.BOLD));
        textView.getPaint().setAntiAlias(true);
        */

        isSaveInstanse = false;

        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled() == false)
        {
            wifiManager.setWifiEnabled(true);
            WifiInfo info = wifiManager.getConnectionInfo();
            if ( !(info.getSSID().startsWith("\""+"TE_")) )
                wifiManager.disconnect();
        }


        wifiListAdapter = new WifiListAdapter(this, TE_wifis);

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                TE_wifis.clear();
                List<ScanResult> results = wifiManager.getScanResults();
                for (ScanResult result : results) {
                    if (result.SSID != null) {
                        int signalStrenght = WifiManager.calculateSignalLevel(result.level, 5);


                        if(result.SSID.startsWith("TE_")) {
                            WifiInfo info = wifiManager.getConnectionInfo();
                            if (info.getSSID().equals("\"" + result.SSID + "\"")) {
                                // Add connected wifi to 0 index
                                TE_wifis.add(0, new WifiListItem(result.SSID, "Connected", signalStrenght));
                            } else
                                TE_wifis.add(new WifiListItem(result.SSID, "", signalStrenght));
                        }
                    }
                }
                wifiListAdapter.notifyDataSetChanged();
            }
        }, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        wifiManager.startScan();


        displayOrientation = getApplicationContext().getResources().getConfiguration().orientation;
        lepton = new Lepton(displayOrientation);

        compassSurface = (SurfaceView) findViewById(R.id.compassSurface);
        compassTextView = (TextView) findViewById(R.id.compassTextView);

        compassView = new CompassView(getApplicationContext(), compassSurface);
        parentViewGroup = (ViewGroup) findViewById(R.id.mainRelLayout);

        clockTextView = (TextView) findViewById(R.id.timeTextView);
        tempTextView = (TextView) findViewById(R.id.tempTextView);
        tempImageView = (ImageView) findViewById(R.id.tempImageView);
        aimImageView = (ImageView) findViewById(R.id.aimImageView);

        toggleRec = (ToggleButton) findViewById(R.id.toggleRec);
        toggleMode = (ToggleButton) findViewById(R.id.toggleMode);

        closeMenuBtn = (ImageView) findViewById(R.id.closeMenuButton);
        takePictureButton = (Button)findViewById(R.id.takePicture);

        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setVisibility(View.INVISIBLE);
//        compassSurface.setVisibility(View.INVISIBLE);
//        compassTextView.setVisibility(View.INVISIBLE);

        mainRelativeLayout = (RelativeLayout)findViewById(R.id.mainRelLayout);



        //
        //
        //
        Clock c=new Clock(this, 0);
        c.AddClockTickListner(new OnClockTickListner() {
            @Override
            public void OnSecondTick(Time currentTime) {
                DateFormat df = new SimpleDateFormat("h:mm");
                String time = df.format(Calendar.getInstance().getTime());
                clockTextView.setText(time);
            }

            @Override
            public void OnMinuteTick(Time currentTime) {
            }
        });

        // Save PNG screenshot
        takePictureButton.setOnClickListener(
                new ImageView.OnClickListener(){
                    public void onClick(View v){
                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd  HH.mm.ss");
                        String date = df.format(Calendar.getInstance().getTime());

                        File myDir = new File(Environment.getExternalStorageDirectory().toString() + "/req_images");
                        myDir.mkdirs();

                        File filePNG;
                        String fname = date + ".png";

                        filePNG = new File(myDir, fname);
                        if (filePNG.exists())
                            filePNG.delete();


                        long prevTime;
                        int d;



                        try {
                            prevTime = System.currentTimeMillis();
                            FileOutputStream out = new FileOutputStream(filePNG);
                            //picture.compress(Bitmap.CompressFormat.JPEG, 95, out);
                            d = (int) (System.currentTimeMillis() - prevTime);
                            //Log.i("screen time", Integer.toString(d));

                            View v1 = getWindow().getDecorView().getRootView();
                            v1.setDrawingCacheEnabled(true);
                            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                            v1.setDrawingCacheEnabled(false);

                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);


                            if(bitmap != null)
                            {
                                bitmap.recycle();
                                bitmap = null;
                            }
                            out.flush();
                            out.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
        );


        //
        //
        //
        if( savedInstanceState != null ) {
            ffmpeg = (myFFmpeg) getLastCustomNonConfigurationInstance();

            recordVideoFmt = savedInstanceState.getInt("recordVideoFmt");
            pallette = savedInstanceState.getInt("pallette");
            isPhotoMode = savedInstanceState.getBoolean("isPhotoMode");
            isRecording = savedInstanceState.getBoolean("isRecording");
            isCompassVisible = savedInstanceState.getBoolean("isCompassVisible");
            isClockVisible = savedInstanceState.getBoolean("isClockVisible");
            cameraXOffset = savedInstanceState.getInt("cameraXOffset");
            cameraYOffset = savedInstanceState.getInt("cameraYOffset");
            cameraScale = savedInstanceState.getFloat("cameraScale");

            if(isRecording) {
                recordVideoStartTimeMillis = savedInstanceState.getLong("recordVideoStartTimeMillis");
            }

            //  Set toggle states after reverting screen
            toggleMode.setChecked(isPhotoMode);
            toggleRec.setChecked(isRecording);
        }
        else {
            ffmpeg = new myFFmpeg(getApplicationContext());
            loadFFMpegBinary();
        }

        /*
        if(isCompassVisible)
            compassView.drawThread.onResume();
        else
            compassView.drawThread.onPause();
        */

        if(isClockVisible)
            clockTextView.setVisibility(View.VISIBLE);
        else
            clockTextView.setVisibility(View.INVISIBLE);



        //
        //  if click on MenuButton open Menu
        //
        findViewById(R.id.MenuImgView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //recView.setVisibility(View.INVISIBLE);

                aimImageView.setVisibility(View.INVISIBLE);
                tempImageView.setVisibility(View.INVISIBLE);
                tempTextView.setVisibility(View.INVISIBLE);
                //
                //  Make recView transparent when menu is opens
                //
                AlphaAnimation alpha = new AlphaAnimation(1.0F, 0.0F);
                alpha.setDuration(0); // Make animation instant
                alpha.setFillAfter(true);
                recView.startAnimation(alpha);


                closeMenuBtn.setClickable(true);
                closeMenuBtn.setVisibility(View.VISIBLE);
                closeMenuBtn.bringToFront();

                addMenuView();

                //
                //  If click on closeButton then close Menu
                //
                closeMenuBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeMenuBtn.setVisibility(View.INVISIBLE);
                        closeMenuBtn.setClickable(false);
                        parentViewGroup.removeView(menuView);
                        parentViewGroup.removeView(subMenuView);
                        //recView.setVisibility(View.VISIBLE);

                        tempImageView.setVisibility(View.VISIBLE);
                        tempTextView.setVisibility(View.VISIBLE);
                        aimImageView.setVisibility(View.VISIBLE);
                        AlphaAnimation alpha = new AlphaAnimation(0.0F, 1.0F);
                        alpha.setDuration(0); // Make animation instant
                        alpha.setFillAfter(true);
                        recView.startAnimation(alpha);
                    }
                });
            }
        });
    }




    // (It's important to note that we can't get the position in the onCreate(),
    // because at that stage most probably the view isn't drawn yet, so it will return (0, 0))
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        // Get the x and y position after the button is draw on screen
        int[] location = new int[2];
        Toolbar toolBar = (Toolbar)findViewById(R.id.toolbar);

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        toolBar.getLocationOnScreen(location);

        //  Initialize the Point with x, and y positions
        //  p.y is a top location of Toolbar
        p = new Point();
        p.x = location[0];
        p.y = location[1] + toolBar.getHeight();


        if(isPhotoMode)
            setPhotoMode();
        else
            setVideoMode();



        //
        //  Open recView layout
        //
        if( recView == null ) {
            recView = getLayoutInflater().inflate(R.layout.rec_layout, parentViewGroup, false);
            recView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            recView.setTranslationX((float) (parentViewGroup.getWidth() - recView.getMeasuredWidth()));
            recView.setTranslationY((float) findViewById(R.id.toolbar).getHeight());

            //  if VideoMode than open recView with video time
            //if(toggleMode.isChecked() == false)
            parentViewGroup.addView(recView);

            if( !toggleMode.isChecked() )
                recView.setVisibility(View.VISIBLE);
            else
                recView.setVisibility(View.INVISIBLE);

            //  if record video turn on RecIndicator
            if( toggleRec.isChecked() )
                findViewById(R.id.recIndicator).setVisibility(View.VISIBLE);
        }

        textRecTime = (TextView) findViewById(R.id.textRecTime);

        //
        //  Check mode: photo or video
        //
        toggleMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    setPhotoMode();
                    //parentViewGroup.removeView(recView);
                    recView.setVisibility(View.INVISIBLE);
                } else {
                    // The toggle is disabled
                   setVideoMode();
                   //parentViewGroup.addView(recView);
                    recView.setVisibility(View.VISIBLE);
                }
            }
        });

        //
        //  Start or stop recording
        //
        toggleRec.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    isRecording = true;
                    findViewById(R.id.recIndicator).setVisibility(View.VISIBLE);

                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");
                    String date = df.format(Calendar.getInstance().getTime());

                    String videoCodec = "";
                    String videoFmt = "";
                    switch (recordVideoFmt) {
                        case 0:
                            videoCodec = "-f flv -s 320x180 ";
                            videoFmt = ".flv";
                            break;
                        case 1:
                            videoCodec = "-f avi -s 320x180 ";
                            videoFmt = ".avi";
                            break;
                        case 2:
                            videoCodec = "-vcodec mpeg4 -s 320x180 ";
                            videoFmt = ".mp4";
                            break;
                    }

                    String cmd = "-f rawvideo -vcodec rawvideo -s 640x360 -r 10 -pix_fmt rgba -i - " + videoCodec + "/sdcard/" + date + videoFmt;

                    execFFmpegBinary(cmd);
                    recordVideoStartTimeMillis = System.currentTimeMillis();
                    recordVideoLastTime = 0;
                } else {
                    // The toggle is disabled
                    isRecording = false;
                    findViewById(R.id.recIndicator).setVisibility(View.INVISIBLE);

                    ffmpeg.onStop();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);



        camera = Camera.open();
        cameraProcessing = new CameraProcessing( getApplicationContext(),
                (SurfaceView) findViewById(R.id.cameraSurfaceView),
                camera,
                lepton.getHorizontalViewAngle(),
                lepton.getVerticalViewAngle(),
                metrics,
                displayOrientation
        );
        cameraProcessing.setCameraOffset(cameraXOffset, cameraYOffset);
        cameraProcessing.setCameraScale(cameraScale);

        FusionParams fp = new FusionParams(surfaceView, cameraProcessing, displayOrientation);

        // Create SurfaceView
        mSurface=(SurfaceView)findViewById(R.id.surfaceView);
        mSurface.getHolder().addCallback(this);


        serverFrameThread = new ServerFrameThread();
        serverFrameThread.start();


        clientCommandThread = new ClientCommandThread();
        clientCommandThread.start();


        if(drawThread != null) {
            drawThread.onResume();
        }


        if( (compassView.drawThread != null) && (isCompassVisible) ) {
            compassView.drawThread.onResume();
        }

        updateUiThread = new UpdateUiThread();
        updateUiThread.start();
    }


    @Override
    protected void onPause(){
        super.onPause();

        cameraXOffset = cameraProcessing.getCameraXOffset();
        cameraYOffset = cameraProcessing.getCameraYOffset();
        cameraScale = cameraProcessing.getCameraScale();

        if(compassView.drawThread != null) {
            compassView.drawThread.onPause();
        }

        if(updateUiThread != null) {
            updateUiThread.interrupt();
            updateUiThread = null;
        }

        if(serverFrameThread != null) {
            serverFrameThread.interrupt();
            serverFrameThread = null;
        }

        if(clientCommandThread != null) {
            clientCommandThread.interrupt();
            clientCommandThread = null;
        }

        if (camera != null)
        {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }

        if(cameraProcessing != null) {
            cameraProcessing = null;
        }
    }


    @Override
    protected void onDestroy(){
        super.onDestroy();
        if(drawThread != null){
            drawThread.interrupt();
            drawThread = null;
        }


        if(compassView.drawThread != null) {
            compassView.drawThread.interrupt();
            compassView.drawThread = null;
        }


        if( !isSaveInstanse ) {
            //  Shut down wifi
            wifiManager.disconnect();
            wifiManager.setWifiEnabled(false);
        }

        if(compassView != null)
            compassView = null;
    }


    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        isSaveInstanse = true;
        outState.putInt("pallette", pallette);
        outState.putInt("recordVideoFmt", recordVideoFmt);
        outState.putBoolean("isRecording", isRecording);
        outState.putBoolean("isPhotoMode", isPhotoMode);
        outState.putBoolean("isClockVisible", isClockVisible);
        outState.putBoolean("isCompassVisible", isCompassVisible);
        outState.putInt("cameraXOffset", cameraXOffset);
        outState.putInt("cameraYOffset", cameraYOffset);
        outState.putFloat("cameraScale", cameraScale);

        if(isRecording) {
            outState.putLong("recordVideoStartTimeMillis", recordVideoStartTimeMillis);
        }
    }


    public Object  onRetainCustomNonConfigurationInstance () {
        return ffmpeg;
    }


    private void loadFFMpegBinary() {
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                }
            });
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }
    }

    private void execFFmpegBinary(final String command) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                }

                @Override
                public void onSuccess(String s) {
                    Log.d("FFmpeg", "SUCCESS with output : "+s);
                }


                @Override
                public void onProgress(String s) {
                    Log.d("FFmpeg", "Started command : ffmpeg " + command);
                }

                @Override
                public void onStart() {
                    Log.d("FFmpeg", "Started command : ffmpeg " + command);
                }

                @Override
                public void onFinish() {
                    Log.d("FFmpeg", "Finished command : ffmpeg " + command);
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // do nothing for now
        }
    }



    private void setPhotoMode() {
        isPhotoMode = true;
        toggleRec.setVisibility(View.INVISIBLE);
        toggleRec.setChecked(false);
        findViewById(R.id.takePicture).setVisibility(View.VISIBLE);
    }


    private void setVideoMode() {
        isPhotoMode = false;
        toggleRec.setVisibility(View.VISIBLE);
        toggleRec.setChecked(isRecording);
        findViewById(R.id.takePicture).setVisibility(View.INVISIBLE);
    }




    class ServerFrameThread extends Thread {
        private short endFrame = 0;
        private short seqNum = 0;
        private int segment = 0;
        private int lenData = 0;
        private int offset = 0;
        private int width = 0;
        private int height = 0;

        private byte[] ByteRxBuf;
        private short[] LeptonTempArray;


        private static final int SERVER_FRAME_PORT = 5004;

        private Socket clientFrameSocket;

        private BufferedInputStream input;
        private BufferedWriter output;
        private byte[] buf;

        public ServerFrameThread() {
            buf = new byte[1400];
            ByteRxBuf = new byte[160*120*2];
            LeptonTempArray = new short[160*120];
        }

        public void run() {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    if(clientFrameSocket == null) {
                        clientFrameSocket = new Socket(InetAddress.getByName("192.168.1.1"), SERVER_FRAME_PORT);
                        input = new BufferedInputStream(clientFrameSocket.getInputStream(), 1400);
                        output = new BufferedWriter(new OutputStreamWriter(clientFrameSocket.getOutputStream()));
                    } else if ( clientFrameSocket.isClosed() ) {
                        clientFrameSocket = new Socket(InetAddress.getByName("192.168.1.1"), SERVER_FRAME_PORT);
                        input = new BufferedInputStream(clientFrameSocket.getInputStream(), 1400);
                        output = new BufferedWriter(new OutputStreamWriter(clientFrameSocket.getOutputStream()));
                    }

                    input.read(buf);
//                    output.write("command=OK");
//                    output.flush();


                    ByteBuffer byteBuffer = ByteBuffer.wrap(buf);

                    // Parse TCP packet
                    endFrame = byteBuffer.getShort(0);
                    offset = byteBuffer.getShort(2);
                    lenData = (int) byteBuffer.getShort(4);
                    segment = (int) byteBuffer.getShort(6);
                    width = (int) byteBuffer.getShort(8);
                    height = (int) byteBuffer.getShort(10);


                    // payload started at 12 byte
                    if( (width == 160) && (height == 120) )
                        System.arraycopy(buf, 12, ByteRxBuf, offset+(segment)*9600, lenData);
                    else if( (width == 80) && (height == 60) )
                        System.arraycopy(buf, 12, ByteRxBuf, offset, lenData);


                    // Frame has been completely received
                    if( (endFrame == 1) && (segment == 3) )
                    {
                        long prevTime;
                        int d;
                        prevTime = System.currentTimeMillis();


                        // Convert Byte[] received buff to Short[]
                        ShortBuffer shortBuf = ByteBuffer.wrap(ByteRxBuf).asShortBuffer();
                        shortBuf.get(LeptonTempArray);

                        lepton.SetImage(LeptonTempArray, width, height);
                        Canvas comboImage;


                        int outBitmapWidth = 0;
                        int outBitmapHeight = 0;
                        if(displayOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                            outBitmapWidth = width;
                            outBitmapHeight = height;
                        }
                        else {
                            outBitmapWidth = height;
                            outBitmapHeight = width;
                        }



                        // Create Bitmaps
                        if( (widthSurface > 0) && (heightSurface > 0) ) {
                            switch (pallette) {
                                case 0:     // Grayscale image
//                                    outBitmap = Bitmap.createBitmap(outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888);
//                                    comboImage = new Canvas(outBitmap);
//                                    comboImage.drawBitmap( Bitmap.createBitmap(lepton.toGray(), outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888), 0f, 0f, null );
                                    Bitmap bitmapUncut = Bitmap.createBitmap(lepton.toGray(), outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888);
                                    Bitmap bitmapCut = Bitmap.createBitmap(lepton.cutRangeImage(), outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888);

                                    // Join BitmapUncut and BitmapCut
                                    outBitmap = Bitmap.createBitmap(outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888);
                                    comboImage = new Canvas(outBitmap);
                                    comboImage.drawBitmap( bitmapUncut, 0f, 0f, null );
                                    comboImage.drawBitmap( bitmapCut, 0f, 0f, null );
                                    break;
                                case 1:     // Invert grayscale image
                                    outBitmap = Bitmap.createBitmap(outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888);
                                    comboImage = new Canvas(outBitmap);
                                    comboImage.drawBitmap( Bitmap.createBitmap(lepton.toInvGray(), outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888), 0f, 0f, null );
                                    break;
                                case 2:     // Rainbow image
                                    outBitmap = Bitmap.createBitmap(outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888);
                                    comboImage = new Canvas(outBitmap);
                                    comboImage.drawBitmap( Bitmap.createBitmap(lepton.toRainbow(), outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888), 0f, 0f, null );
                                    break;
                                case 3:     // Ironbow image
                                    outBitmap = Bitmap.createBitmap(outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888);
                                    comboImage = new Canvas(outBitmap);
                                    comboImage.drawBitmap( Bitmap.createBitmap(lepton.toIronbow(), outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888), 0f, 0f, null );
                                    break;
                                case 4:     // Ironbow image
                                    outBitmap = Bitmap.createBitmap(outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888);
                                    comboImage = new Canvas(outBitmap);
                                    comboImage.drawBitmap( Bitmap.createBitmap(lepton.toIronbow(), outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888), 0f, 0f, null );
                                    break;
                            }

                            if(cameraProcessing != null)
                                cameraProcessing.setBitmap(outBitmap);
                            //  Interpolate bitmap to 160x120 from 80x60
                            if( (width == 80) && (height == 60) )
                                outBitmap = Bitmap.createScaledBitmap(outBitmap, outBitmapWidth*2, outBitmapHeight*2, true);



                            d = (int) (System.currentTimeMillis() - prevTime);
                            //Log.i("interpTime", Integer.toString(d));

                            if(drawThread != null)
                                drawThread.onResume();
                        }
                    }
                }
//                output.write("command=STOP_SOCKET");
//                output.flush();
//
//                if(clientFrameSocket != null) {
//                    clientFrameSocket.shutdownOutput();
//                    clientFrameSocket.shutdownInput();
//                    clientFrameSocket.close();
//                }
            } catch (IOException e) {
                e.printStackTrace();
            }


            try {
                if(clientFrameSocket != null)
                    clientFrameSocket.close();

                //if(serverFrameSocket != null)
                    //serverFrameSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class UpdateUiThread extends Thread {
        private int prevCalibrationState = 0;
        private int openMenu = 0;

        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    if(clientCommandThread != null)
                        clientCommandThread.sendCommand("GET_MAIN_PARAMS");

                    if( (isCompassCalibrate) && (compass != null) && (compass.getCalibrationState() != prevCalibrationState) ) {
                        prevCalibrationState = compass.getCalibrationState();
                        if(prevCalibrationState == 2) {
                            prevCalibrationState = 0;
                            isCompassCalibrate = false;
                            clientCommandThread.sendCommand("STOP_CALIBRATE_COMPASS");
                        }
                    }

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            tempTextView.setText( String.format("%.1f", lepton.aimTemp) );

                            //compassTextView.setText( String.format("%.0f", azimuthInDegress) );
                            if(compassView != null)
                                compassTextView.setText( compassView.getCompassString() );


                            if(isRecording) {
                                long recordVideoTime = (System.currentTimeMillis() - recordVideoStartTimeMillis)/1000;

                                if( (recordVideoTime-recordVideoLastTime) >= 1) {
                                    recordVideoLastTime = recordVideoTime;
                                    int seconds = (int) (recordVideoTime) % 60;
                                    int minutes = (int) ((recordVideoTime / 60) % 60);
                                    int hours = (int) ((recordVideoTime / 3600) % 24);
                                    String time = String.format("%02d:%02d:%02d", hours, minutes, seconds);

                                    if(textRecTime != null)
                                        textRecTime.setText(time);
                                }
                            }

                            if(openMenu != 0) {
                                switch (openMenu){
                                    case R.layout.calibrate_compass_layout:
                                        addCompassView();
                                        break;
                                    case R.layout.settings_layout:
                                        addSettingsView();
                                        break;
                                }
                                openMenu = 0;
                            }
                        }
                    });
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        void openMenu (int menuID) {openMenu = menuID;}
    }


    //***********************************************************
    //******************* TCP Command Thread ***********************
    //***********************************************************
    // receive aim, min, max temperatures
    // battery charge and voltage
    class ClientCommandThread extends Thread {
        private static final int SERVER_DATA_PORT = 5005;

        private Socket clientDataSocket;

        private BufferedReader input;
        private BufferedWriter output;


        private ArrayList<String> inParametersList = new ArrayList<String>();
        private ArrayList<String> outCommands = new ArrayList<String>();

        private int fpaTemp;
        private int auxTemp;
        private float busVoltage;
        private float busCurrent;
        private int timestamp;
        private float[] mLastAccelerometer = new float[3];
        private float[] mLastMagnetometer = new float[3];
        private float[] mLastGyroscope = new float[3];


        public ClientCommandThread() {
        }

        public void run() {
            try {
                while(!Thread.currentThread().isInterrupted()) {
                    if(clientDataSocket == null) {
                        clientDataSocket = new Socket(InetAddress.getByName("192.168.1.1"), SERVER_DATA_PORT);
                        input = new BufferedReader(new InputStreamReader(clientDataSocket.getInputStream()));
                        output = new BufferedWriter(new OutputStreamWriter(clientDataSocket.getOutputStream()));
                    } else if ( clientDataSocket.isClosed() ) {
                        clientDataSocket = new Socket(InetAddress.getByName("192.168.1.1"), SERVER_DATA_PORT);
                        input = new BufferedReader(new InputStreamReader(clientDataSocket.getInputStream()));
                        output = new BufferedWriter(new OutputStreamWriter(clientDataSocket.getOutputStream()));
                    }

                    if(outCommands.size() > 0) {
                        output.write(outCommands.get(0));
                        output.flush();
                        outCommands.remove(0);
                        outCommands.trimToSize();

                        //  Waiting response
                        char[] inBuf = new char[1400];
                        input.read(inBuf);
                        parseInCommand(new String(inBuf));
                        if( inParametersList.size() > 0 )
                            processCommand();
                    } else {
                        SystemClock.sleep(1);
                    }
                }
                if(clientDataSocket != null)
                    clientDataSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void parseInCommand(String cmd) {
            //Log.i("response", cmd);
            inParametersList.clear();
            inParametersList.trimToSize();
            int charsToEnd = cmd.length();
            int cmdOff = 0;
            int endLineOff = 0;
            int lineOff = 0;

            while(charsToEnd > 0) {
                cmdOff = cmd.indexOf('=', lineOff);
                endLineOff = cmd.indexOf('\n', lineOff);
                lineOff = endLineOff + 1;
                charsToEnd = cmd.length() - lineOff;

                if( (cmdOff<=-1) || (endLineOff<=-1) )
                    break;
                inParametersList.add(cmd.substring(cmdOff+1, endLineOff));
            }
        }

        private void processCommand() {

            if( inParametersList.get(0).equals("GET_MAIN_PARAMS") ) {
                // Parse TCP packet
                try {
                    fpaTemp = Integer.parseInt(inParametersList.get(1));
                    auxTemp = Integer.parseInt(inParametersList.get(2));
                    busVoltage = Float.intBitsToFloat( Integer.parseInt(inParametersList.get(3)) );
                    busCurrent = Float.intBitsToFloat( Integer.parseInt(inParametersList.get(4)) );
                    timestamp = Integer.parseInt(inParametersList.get(6));

                    mLastAccelerometer[0] = Float.intBitsToFloat( Integer.parseInt(inParametersList.get(8)) ) * 9.8f;
                    mLastAccelerometer[1] = Float.intBitsToFloat( Integer.parseInt(inParametersList.get(9)) ) * 9.8f;
                    mLastAccelerometer[2] = Float.intBitsToFloat( Integer.parseInt(inParametersList.get(7)) ) * 9.8f;
                    mLastMagnetometer[0] = Float.intBitsToFloat( Integer.parseInt(inParametersList.get(11)) );
                    mLastMagnetometer[1] = Float.intBitsToFloat( Integer.parseInt(inParametersList.get(12)) );
                    mLastMagnetometer[2] = Float.intBitsToFloat( Integer.parseInt(inParametersList.get(10)) );
                    mLastGyroscope[0] = Float.intBitsToFloat( Integer.parseInt(inParametersList.get(14)) );
                    mLastGyroscope[1] = -Float.intBitsToFloat( Integer.parseInt(inParametersList.get(15)) );
                    mLastGyroscope[2] = Float.intBitsToFloat( Integer.parseInt(inParametersList.get(13)) );
                    deltaT = Float.intBitsToFloat( Integer.parseInt(inParametersList.get(16)) );
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                lepton.tempAUX = ((float) auxTemp) / 100f;
                lepton.tempFPA = ((float) fpaTemp) / 100f;
                lepton.setShutterAuxDT(deltaT);

                compass.setSensors(mLastAccelerometer, mLastMagnetometer, mLastGyroscope, timestamp);

                float azimuthInRadians = compass.getAzimuth();
                azimuthInDegress = (float)(Math.toDegrees(azimuthInRadians)+360)%360;
                if(compassView != null)
                    compassView.updateAzimuth(azimuthInDegress);

                return;
            }

            if( inParametersList.get(0).equals("RUN_CALIBRATE_COMPASS") ) {
                compass.runCalibration();
                isCompassCalibrate = true;
                updateUiThread.openMenu(R.layout.calibrate_compass_layout);
            }

            if( inParametersList.get(0).equals("STOP_CALIBRATE_COMPASS") ) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i<mainRelativeLayout.getChildCount(); i++)
                            mainRelativeLayout.getChildAt(i).setVisibility(View.VISIBLE);
                        compassView.drawThread.onResume();
                    }
                });
                updateUiThread.openMenu(R.layout.settings_layout);
            }

            if( inParametersList.get(0).equals("GET_FIRMWARE_VERSION") ) {
                final int fwVersion = Integer.parseInt(inParametersList.get(1));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView fwVersionTextView = (TextView) subMenuView.findViewById(R.id.fwVersionTextView);
                        fwVersionTextView.setText( "ThirdEYE Version = " + String.valueOf(fwVersion) );
                    }
                });
            }

            if( inParametersList.get(0).equals("RUN_UPDATE_FIRMWARE") ) {
                ClientUpdateThread clientUpdateThread = new ClientUpdateThread();
                clientUpdateThread.start();
            }
        }

        public void sendCommand(String cmd, ArrayList<Integer> params) {
            String outMessage = "command=" + cmd + System.getProperty("line.separator");


            for (Integer param : params) {

            }
            outCommands.add(outMessage);
        }


        public void sendCommand(String cmd) {
            String outMessage = "command=" + cmd + System.getProperty("line.separator");
            outCommands.add(outMessage);
        }
    }


    //***********************************************************
    //******************* TCP Data Thread ***********************
    //***********************************************************
    class ClientUpdateThread extends Thread {
        private static final int SERVER_DATA_PORT = 5004;

        private Socket clientDataSocket;

        private BufferedOutputStream output;
        private BufferedReader input;
        private int sha1BlockLenght = 64;
        private int sha1HashLenght = 20;
        private int payloadLenght = 0;
        private int packetLenght = 0;
        private int numOfPacket = 0;

        //  Header
        private int totalNumOfPackets = 0;
        private int offset = 0;
        private int len = 0;
        private int totalLenght = 0;
        private int sha1Marker = 0;
        private int headerLenght = 20;

        public ClientUpdateThread() {
            openTeFirmwareFile();
            totalLenght = cc3200UpdateFile.length - sha1HashLenght;
            payloadLenght = ( 1400/sha1BlockLenght ) * sha1BlockLenght;
            packetLenght = payloadLenght + headerLenght;
            totalNumOfPackets = (cc3200UpdateFile.length-sha1HashLenght) / payloadLenght + 2;    // 2 - last packet and sha1 packet
        }

        public void run() {
            final int timeOut = 100; // 100 ms wait period

            while(true) {
                try {
                    clientDataSocket = new Socket();
                    clientDataSocket.connect(new InetSocketAddress(InetAddress.getByName("192.168.1.1"), SERVER_DATA_PORT), timeOut);
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                    clientDataSocket = null;
                }
            }

            try {
//                clientDataSocket = new Socket(InetAddress.getByName("192.168.1.1"), SERVER_DATA_PORT);
                output = new BufferedOutputStream(clientDataSocket.getOutputStream(), packetLenght);
                input = new BufferedReader(new InputStreamReader(clientDataSocket.getInputStream()));

                char[] buf = new char[30];

                sendFile();
                input.read(buf);
                Log.i("Update", new String(buf));

                sendFile();

                if(clientDataSocket != null)
                    clientDataSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        private void sendFile() {
            // Send firmware
            numOfPacket = 1;
            len = 0;
            offset = 0;
            sha1Marker = 0;
            while ( (!Thread.currentThread().isInterrupted()) && (totalNumOfPackets-1 >= numOfPacket) ) {
                if( (totalLenght-offset) >= (payloadLenght) )
                    len = payloadLenght;
                else
                    len = totalLenght-offset;


                ByteBuffer bb = ByteBuffer.allocate(packetLenght);
                bb.putInt(totalNumOfPackets);
                bb.putInt(totalLenght);
                bb.putInt(offset);
                bb.putInt(len);
                bb.putInt(sha1Marker);
                bb.put(cc3200UpdateFile, offset, len);
                try {
                    output.write(bb.array(), 0, packetLenght);
                    output.flush();

                    // recieve request from TE
                    char[] buf = new char[30];
                    input.read(buf);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                offset += len;
                numOfPacket++;
            }

            // Send Hash
            sha1Marker = 1;
            len = sha1HashLenght;
            offset = 0;
            ByteBuffer bb = ByteBuffer.allocate(packetLenght);
            bb.putInt(totalNumOfPackets);
            bb.putInt(totalLenght);
            bb.putInt(offset);
            bb.putInt(len);
            bb.putInt(sha1Marker);
            bb.put(cc3200UpdateFile, cc3200UpdateFile.length-sha1HashLenght, len);
            try{
                output.write(bb.array(), 0, packetLenght);
                output.flush();

                // recieve request from TE
                char[] buf = new char[30];
                input.read(buf);
                Log.i("Update", new String(buf));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }



    //***********************************************************
    //******************** SurfaceView  *************************
    //***********************************************************

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        drawThread = new DrawThread(holder, getResources());
        drawThread.start();
        Log.i("SurfaceView", "Created");
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if(drawThread != null) {
            drawThread.interrupt();
            drawThread = null;
        }
    }

    class DrawThread extends Thread{
        private boolean runFlag = false;
        private SurfaceHolder surfaceHolder;
        private Matrix matrix;
        private Paint paint;
        private float scaleWidth;
        private float scaleHeight;

        public DrawThread(SurfaceHolder surfaceHolder, Resources resources) {
            this.surfaceHolder = surfaceHolder;

            /*
            // загружаем картинку, которую будем отрисовывать
            picture = BitmapFactory.decodeResource(resources, R.drawable.ic_launcher);

            // формируем матрицу преобразований для картинки
            matrix = new Matrix();
            matrix.postScale(3.0f, 3.0f);
            matrix.postTranslate(100.0f, 100.0f);
            */

            outBitmap = Bitmap.createBitmap(lepton.toRainbow(), 80, 60, Bitmap.Config.RGB_565);
            outBitmap = Bitmap.createScaledBitmap(outBitmap, 160, 120, true);



            if( displayOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                // Get width & height screen
                widthSurface = getWindow().getDecorView().getWidth();
                heightSurface = widthSurface * 3 / 4;

                //heightSurface = getWindow().getDecorView().getHeight();
                scaleWidth = ((float) widthSurface) / outBitmap.getWidth();
                scaleHeight = ((float) heightSurface) / outBitmap.getHeight();

                matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);
            }
            else
            {
                // Get width & height screen
                widthSurface = getWindow().getDecorView().getHeight();
                heightSurface = widthSurface * 3 / 4;

                scaleWidth = ((float) widthSurface) / outBitmap.getWidth();
                scaleHeight = ((float) heightSurface) / outBitmap.getHeight();

                matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);
                //matrix.postTranslate( getWindow().getDecorView().getWidth(), 0);

                //matrix.preRotate(90);
            }

            paint = new Paint();
            paint.setFilterBitmap(true);
            paint.setAntiAlias(true);
        }

        @Override
        public void run() {
            Canvas dispCanvas;
            Bitmap bitmap;
            Bitmap camBitmap;
            Canvas canvas;
            Bitmap recBitmap;
            Canvas recCanvas;

            // Wait while camera starting
            while( (cameraProcessing == null) )
                SystemClock.sleep(1);
            while( !cameraProcessing.isInitialized() )
                SystemClock.sleep(1);

            int cameraWidth = cameraProcessing.getCameraWidth();
            int cameraHeight = cameraProcessing.getCameraHeight();
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);

            if (displayOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                bitmap = Bitmap.createBitmap(cameraWidth, cameraWidth * metrics.heightPixels / metrics.widthPixels, Bitmap.Config.ARGB_8888);
            } else {
                bitmap = Bitmap.createBitmap(cameraWidth * metrics.widthPixels / metrics.heightPixels, cameraWidth, Bitmap.Config.ARGB_8888);
            }
            recBitmap = Bitmap.createBitmap(cameraWidth, cameraWidth * metrics.widthPixels / metrics.heightPixels, Bitmap.Config.ARGB_8888);
            recCanvas = new Canvas(recBitmap);
            canvas = new Canvas(bitmap);


            while (!Thread.currentThread().isInterrupted()) {
                dispCanvas = null;

                try {
                    // получаем объект Canvas и выполняем отрисовку
                    onPause();

                    while(!runFlag)
                        SystemClock.sleep(1);


                    dispCanvas = surfaceHolder.lockCanvas(null);


                    long prevTime;
                    int d;
                    prevTime = System.currentTimeMillis();
                    synchronized (surfaceHolder) {
                        if ((outBitmap != null) && (dispCanvas != null)) {
                            if (displayOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                                Rect leptRectSrc, camRectSrc, leptRectDst, RectSrc, RectDst;

                                leptRectSrc = new Rect(0, outBitmap.getHeight() - outBitmap.getWidth() * dispCanvas.getHeight() / dispCanvas.getWidth(), outBitmap.getWidth(), outBitmap.getHeight());
                                leptRectDst = new Rect(0, 0, canvas.getWidth(), canvas.getHeight());

                                camRectSrc = new Rect(cameraProcessing.getCameraYOffset(),
                                        cameraHeight - (int) ((cameraWidth * dispCanvas.getHeight() / dispCanvas.getWidth()) / cameraProcessing.getCameraScale()) - cameraProcessing.getCameraXOffset(),
                                        (int) (cameraWidth / cameraProcessing.getCameraScale()) + cameraProcessing.getCameraYOffset(),
                                        (int) cameraHeight - cameraProcessing.getCameraXOffset()
                                );

                                paint.setFilterBitmap(true);
                                canvas.drawBitmap(outBitmap, leptRectSrc, leptRectDst, paint);
                                //dispCanvas.drawBitmap(outBitmap, matrix, paint);
                                if ((cameraProcessing != null) && (pallette == 4)) {
                                    camBitmap = cameraProcessing.getBitmap();
                                    if (camBitmap != null) {
                                        paint.setFilterBitmap(true);
                                        canvas.drawBitmap(camBitmap, camRectSrc, leptRectDst, paint);
                                    }
                                }
                                if(isRecording) {
                                    ffmpeg.sendBitmapToStream(bitmap);
                                }

                                paint.setFilterBitmap(false);

                                RectSrc = leptRectDst;
                                RectDst = new Rect(0, 0, dispCanvas.getWidth(), dispCanvas.getHeight());
                                dispCanvas.drawBitmap(bitmap, RectSrc, RectDst, paint);
                            } else {
                                Rect leptRectSrc, camRectSrc, leptRectDst, RectSrc, RectDst;

                                leptRectSrc = new Rect(0, 0, outBitmap.getHeight() * dispCanvas.getWidth() / dispCanvas.getHeight(), outBitmap.getHeight());
                                leptRectDst = new Rect(0, 0, canvas.getWidth(), canvas.getHeight());

                                camRectSrc = new Rect(cameraProcessing.getCameraXOffset(),
                                        cameraProcessing.getCameraYOffset(),
                                        (int) ((cameraWidth * dispCanvas.getWidth() / dispCanvas.getHeight()) / cameraProcessing.getCameraScale()) + cameraProcessing.getCameraXOffset(),
                                        (int) (cameraWidth / cameraProcessing.getCameraScale()) + cameraProcessing.getCameraYOffset()
                                );

                                paint.setFilterBitmap(true);
                                canvas.drawBitmap(outBitmap, leptRectSrc, leptRectDst, paint);
                                if ((cameraProcessing != null) && (pallette == 4)) {
                                    camBitmap = cameraProcessing.getBitmap();
                                    if (camBitmap != null) {
                                        paint.setFilterBitmap(true);
                                        canvas.drawBitmap(camBitmap, camRectSrc, leptRectDst, paint);
                                    }
                                }

                                if(isRecording) {
                                    Matrix matr = new Matrix();
                                    matr.setRotate(90f);
                                    matr.postTranslate(640.0f, 0.0f);

                                    paint.setFilterBitmap(false);
                                    recCanvas.drawBitmap(bitmap, matr, paint);
                                    ffmpeg.sendBitmapToStream(recBitmap);
                                }

                                paint.setFilterBitmap(false);

                                RectSrc = leptRectDst;
                                RectDst = new Rect(0, 0, dispCanvas.getWidth(), dispCanvas.getHeight());
                                dispCanvas.drawBitmap(bitmap, RectSrc, RectDst, paint);
                            }
                        }
                        d = (int) (System.currentTimeMillis() - prevTime);
                        //Log.i("sleep time", Integer.toString(d));
                    }

                } finally {
                    if (dispCanvas != null) {
                        // отрисовка выполнена. выводим результат на экран
                        surfaceHolder.unlockCanvasAndPost(dispCanvas);
                    }
                }
            }
        }

        /**
         * Call this on pause.
         */
        public void onPause() {
            runFlag = false;
        }

        /**
         * Call this on resume.
         */
        public void onResume() {
            runFlag = true;
        }
    }



    //***********************************************************
    //*********************ThirdEYE Firmware*********************
    //***********************************************************
    private void openTeFirmwareFile () {
        //*Don't* hardcode "/sdcard"
        File sdcard = Environment.getExternalStorageDirectory();

        //Get the text file
        File file = new File(sdcard,"wlan_ap.bin");
        FileInputStream fin = null;
        try {
            // create FileInputStream object
            fin = new FileInputStream(file);

            cc3200UpdateFile = new byte[(int)file.length()];

            // Reads up to certain bytes of data from this input stream into an array of bytes.
            fin.read(cc3200UpdateFile);

//            for(int i = 0; i < cc3200UpdateFile.length; i++)
//            {
//                cc3200UpdateFile[i] = (byte) (i % 16);
//            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            // close the streams using close method
            try {
                if (fin != null) {
                    fin.close();
                }
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }




    //***********************************************************
    //******************* Open Main Menu ************************
    //***********************************************************
    private void addMenuView () {
        menuView = LayoutInflater.from(getBaseContext()).inflate(R.layout.popup_layout, parentViewGroup, false);
        menuView.setTranslationX((float)p.x);
        menuView.setTranslationY((float)p.y);


        parentViewGroup.removeView(subMenuView);
        parentViewGroup.addView(menuView);


        RadioGroup radiogroup = (RadioGroup) findViewById(R.id.radioMenuSelector);

        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                switch (checkedId) {
                    case -1:
                        break;
                    case R.id.radioDispMode:
                        addDispModeView();
                        break;
                    case R.id.radioData:
                        addDataView();
                        break;
                    case R.id.radioRecording:
                        addRecordingView();
                        break;
                    case R.id.radioFusion:
                        addFusionView();
                        break;
                    case R.id.radioSettings:
                        addSettingsView();
                        break;

                    default:
                        break;
                }
            }
        });
    }



    //***********************************************************
    //******************* Menu Items ****************************
    //***********************************************************
    private void addDispModeView () {
        addSubMenuView(R.layout.dispmode_layout);
        if (displayOrientation == Configuration.ORIENTATION_PORTRAIT) {
            subMenuView.findViewById(R.id.dispmodeBackBtn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addMenuView();
                }
            });
        }

        RadioGroup radiogroup = (RadioGroup) findViewById(R.id.groupDispMode);
        switch (pallette) {
            case 0:     // Grayscale image
                radiogroup.check(R.id.radioBlackBlue);
                break;
            case 1:     // Invert grayscale image
                radiogroup.check(R.id.radioBlackWhite);
                break;
            case 3:     // Rainbow image
                radiogroup.check(R.id.radioColor);
                break;
            case 4:     // Ironbow image
                radiogroup.check(R.id.radioFusion);
                break;
        }

        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                switch (checkedId) {
                    case -1:
                        break;
                    case R.id.radioBlackBlue:
                        pallette = 0;
                        break;
                    case R.id.radioBlackWhite:
                        pallette = 1;
                        break;
                    case R.id.radioColor:
                        pallette = 3;
                        break;
                    case R.id.radioFusion:
                        pallette = 4;
                        break;

                    default:
                        break;
                }
            }
        });
    }

    private void addDataView () {
        addSubMenuView(R.layout.data_layout);
        if (displayOrientation == Configuration.ORIENTATION_PORTRAIT) {
            subMenuView.findViewById(R.id.dataBackBtn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addMenuView();
                }
            });
        }

        CheckBox checkVisibleClock;
        CheckBox checkVisibleCompass;

        checkVisibleClock = (CheckBox) findViewById(R.id.checkVisibleClock);
        checkVisibleCompass = (CheckBox) findViewById(R.id.checkVisibleCompass);

        checkVisibleClock.setChecked(isClockVisible);
        checkVisibleCompass.setChecked(isCompassVisible);

        checkVisibleClock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                 if(isChecked) {
                     isClockVisible = true;
                     clockTextView.setVisibility(View.VISIBLE);
                 }
                 else {
                     isClockVisible = false;
                     clockTextView.setVisibility(View.INVISIBLE);
                 }
             }
        });


        checkVisibleCompass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                           @Override
                                                           public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
               if(isChecked) {
                   isCompassVisible = true;
                   compassTextView.setVisibility(View.VISIBLE);
                   compassView.drawThread.onResume();
               }
               else {
                   isCompassVisible = false;
                   compassTextView.setVisibility(View.INVISIBLE);
                   compassView.drawThread.onPause();
               }
           }
       });
    }

    private void addRecordingView () {
        addSubMenuView(R.layout.recording_layout);
        if (displayOrientation == Configuration.ORIENTATION_PORTRAIT) {
            subMenuView.findViewById(R.id.recordingBackBtn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addMenuView();
                }
            });
        }

        RadioGroup radiogroup = (RadioGroup) findViewById(R.id.groupRecordFmt);
        switch (recordVideoFmt) {
            case 0:
                radiogroup.check(R.id.radioFlv);
                break;
            case 1:
                radiogroup.check(R.id.radioAvi);
                break;
            case 2:
                radiogroup.check(R.id.radioMp4);
                break;
        }

        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                switch (checkedId) {
                    case -1:
                        break;
                    case R.id.radioFlv:
                        recordVideoFmt = 0;
                        break;
                    case R.id.radioAvi:
                        recordVideoFmt = 1;
                        break;
                    case R.id.radioMp4:
                        recordVideoFmt = 2;
                        break;

                    default:
                        break;
                }
            }
        });
    }

    private void addFusionView () {
        addSubMenuView(R.layout.fusion_layout);

        if(displayOrientation == Configuration.ORIENTATION_PORTRAIT)
            subMenuView.findViewById(R.id.fusionBackBtn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addMenuView();
                }
            });
    }

    private void addSettingsView () {
        addSubMenuView(R.layout.settings_layout);

        if (displayOrientation == Configuration.ORIENTATION_PORTRAIT) {
            subMenuView.findViewById(R.id.settingsBackBtn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addMenuView();
                }
            });
        }

        subMenuView.findViewById(R.id.calibrateCompassBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clientCommandThread.sendCommand("RUN_CALIBRATE_COMPASS");
            }
        });

        RadioGroup radiogroup = (RadioGroup) findViewById(R.id.radioSettingsSelector);

        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                switch (checkedId) {
                    case -1:
                        break;
                    case R.id.radioCommunication:
                        addCommunicationView();
                        break;
                    case R.id.radioTeCalibration:
                        addTecalibrationView();
                        break;
                    case R.id.radioUpdate:
                        addUpdateView();
                        break;

                    default:
                        break;
                }
            }
        });
    }


    //***********************************************************
    //******************* Settings Items ************************
    //***********************************************************
    private void addCommunicationView () {
        addSubMenuView(R.layout.communication_layout);

        wifiListView = (ListView) findViewById(R.id.wifiListView);

        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiListView.setAdapter(wifiListAdapter);

        wifiManager.startScan();

        wifiListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                if(serverFrameThread != null) {
                    serverFrameThread.interrupt();
                    serverFrameThread = null;
                }

                if(clientCommandThread != null) {
                    clientCommandThread.interrupt();
                    clientCommandThread = null;
                }

                String networkSSID = ((WifiListItem)wifiListAdapter.getItem(position)).ssid;

                WifiConfiguration conf = new WifiConfiguration();
                conf.SSID = "\"" + networkSSID + "\"";   // Please note the quotes. String should contain ssid in quotes
                //conf.preSharedKey = "\""+ networkPass +"\"";
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                wifiManager.addNetwork(conf);


                List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
                for( WifiConfiguration i : list ) {
                    if(i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                        wifiManager.disconnect();
                        wifiManager.enableNetwork(i.networkId, true);
                        wifiManager.reconnect();
                        break;
                    }
                }

                // This is fucking shit !!
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        serverFrameThread = new ServerFrameThread();
                        serverFrameThread.start();

                        clientCommandThread = new ClientCommandThread();
                        clientCommandThread.start();
                    }
                }, 2000);
            }
        });

        subMenuView.findViewById(R.id.communicationBackBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSettingsView();
            }
        });

    }

    private void addTecalibrationView () {
        addSubMenuView(R.layout.tecalibration_layout);

        subMenuView.findViewById(R.id.tecalibrationBackBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSettingsView();
            }
        });
    }

    private void addCompassView () {
        for (int i=0; i<mainRelativeLayout.getChildCount(); i++)
            mainRelativeLayout.getChildAt(i).setVisibility(View.INVISIBLE);
        //mainRelativeLayout.setVisibility(View.INVISIBLE);

        surfaceView.setVisibility(View.VISIBLE);
        compassView.drawThread.onPause();
        addSubMenuView(R.layout.calibrate_compass_layout);

    }

    private void addUpdateView () {
        addSubMenuView(R.layout.update_layout);
        clientCommandThread.sendCommand("GET_FIRMWARE_VERSION");

        subMenuView.findViewById(R.id.updateBackBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSettingsView();
            }
        });

        subMenuView.findViewById(R.id.updateTeBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serverFrameThread.interrupt();
                serverFrameThread = null;
                clientCommandThread.sendCommand("RUN_UPDATE_FIRMWARE");
            }
        });
    }



    // This function adds submenu to display
    private void addSubMenuView (int view) {
        if (displayOrientation == Configuration.ORIENTATION_PORTRAIT) {
            parentViewGroup.removeView(menuView);
            parentViewGroup.removeView(subMenuView);

            subMenuView = LayoutInflater.from(getBaseContext()).inflate(view, parentViewGroup, false);

            subMenuView.setTranslationX((float) p.x);
            subMenuView.setTranslationY((float) p.y);

            parentViewGroup.addView(subMenuView);
        }
        else {
            parentViewGroup.removeView(subMenuView);

            subMenuView = LayoutInflater.from(getBaseContext()).inflate(view, parentViewGroup, false);

            subMenuView.setTranslationX( (float) (p.x + menuView.getWidth()) );
            subMenuView.setTranslationY((float) p.y );

            parentViewGroup.addView(subMenuView);
        }
    }

}
